---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpRole.md
schema: 2.0.0
---

# Get-PulpRole

## SYNOPSIS
Get Pulp roles.

## SYNTAX

### Strings (Default)
```
Get-PulpRole [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-Id] <String[]>] [[-DisplayName] <String>] [-Description <String>] [<CommonParameters>]
```

### Objects
```
Get-PulpRole [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -User <Object[]> [<CommonParameters>]
```

## DESCRIPTION
This function returns Pulp roles. 

The following may be supplied:

- One or more role IDs (wildcards accepted).
- One or more Pulp user objects.  Roles containing the users will be returned.

Roles can also be filtered by role display name and description.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpUser grace | Get-PulpRole
```

Get all roles with grace as a member.

### Example 2
```
PS C:\> Get-PulpRole -Id developers
```

Get the developers role.

### Example 3
```
PS C:\> Get-PulpRole -Description 'qa*'
```

Get all roles with description matching `qa*`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Id
Role IDs.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: False
Position: 1
Default value: @('*')
Accept pipeline input: True (ByValue)
Accept wildcard characters: True
```

### -User
Pulp user objects.

```yaml
Type: Object[]
Parameter Sets: Objects
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -DisplayName
Role display name.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: 2
Default value: *
Accept pipeline input: False
Accept wildcard characters: True
```

### -Description
Role description.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: *
Accept pipeline input: False
Accept wildcard characters: True
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp user objects.

### System.String[]
Pulp role IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp role objects.

## NOTES
NAME:  Get-PulpRole

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
