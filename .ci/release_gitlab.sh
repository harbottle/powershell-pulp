#!/bin/bash
pip install gitlab_release
shopt -s extglob
python -m gitlab_release --description "**DEB files for installation on Debian, Ubuntu, etc.**" --private-token $release_token *.deb
python -m gitlab_release --description "**MSI files for installation on Windows**" --private-token $release_token *.msi
python -m gitlab_release --description "**RPM files for installation on RHEL, CentOS, Fedora, etc.**" --private-token $release_token !(*.src).rpm
python -m gitlab_release --description "**Source RPM files**" --private-token $release_token *.src.rpm
