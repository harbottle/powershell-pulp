---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Remove-PulpRoleMember.md
schema: 2.0.0
---

# Remove-PulpRoleMember

## SYNOPSIS
This function removes one or more users from one or more Pulp roles.

## SYNTAX

### UserStringsRoleStrings (Default)
```
Remove-PulpRoleMember [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-UserLogin] <String[]> -RoleId <String[]> [<CommonParameters>]
```

### UserObjectsRoleStrings
```
Remove-PulpRoleMember [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-User] <PSObject[]> -RoleId <String[]> [<CommonParameters>]
```

### UserObjectsRoleObjects
```
Remove-PulpRoleMember [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-User] <PSObject[]> -Role <PSObject[]> [<CommonParameters>]
```

### UserStringsRoleObjects
```
Remove-PulpRoleMember [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -Role <PSObject[]> [-UserLogin] <String[]> [<CommonParameters>]
```

## DESCRIPTION
This function adds one or more users from one or more roles on a Pulp server.

At least one user must be supplied (as objects or strings)

At least one role must be supplied (as objects or strings)

## EXAMPLES

### Example 1
```
Get-PulpUser "da-*" | Remove-PulpRoleMember -RoleId editors
```

Removes all users with login name starting "da" from role "editors".

## PARAMETERS

### -Server
The Pulp server to query.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Server).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
The port to query.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
The protocol to use.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -User
User object/s containing login property.

```yaml
Type: PSObject[]
Parameter Sets: UserObjectsRoleStrings, UserObjectsRoleObjects
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Role
Role objects/s containing id property.

```yaml
Type: PSObject[]
Parameter Sets: UserObjectsRoleObjects, UserStringsRoleObjects
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -UserLogin
User login string/s.

```yaml
Type: String[]
Parameter Sets: UserStringsRoleStrings, UserStringsRoleObjects
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -RoleId
Role id string/s.

```yaml
Type: String[]
Parameter Sets: UserStringsRoleStrings, UserObjectsRoleStrings
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### `System.Management.Automation.PSCustomObject[]`
Provide one or more objects representing Pulp users. 
Objects must contain \`login\` properties.

## OUTPUTS

### None

## NOTES
NAME:  Remove-PulpRoleMember

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
