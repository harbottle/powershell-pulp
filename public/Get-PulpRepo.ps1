# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpRepo {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,
    
    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,
    
    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true)]
    [string[]]$Id = "*",
    
    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {
    $uri = "/pulp/api/v2/repositories/?details=true&importers=true&distributors=true"
    $repos = Invoke-PulpRestMethod -Server $Server -Port $Port `
             -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
             -Uri $uri
  }
  Process {
    foreach ($item in $Id) {
      If ($Type) {
        $repos | Where-Object {($_.notes.'_repo-type' -eq  "${Type}-repo") -and
                               ($_.id -like $item)}
      }
      Else {
        $repos | Where-Object {($_.id -like $item)}
      }
    }
  }
}
