# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpTask {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Task,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$TaskId,

    [Parameter(Mandatory=$false)]
    [switch]$NoSpawnedTasks,

    [Parameter(Mandatory=$false)]
    [switch]$All
  )
  Begin {
    if (!$Task -and !$TaskID){
      $uri = "/pulp/api/v2/tasks/search/"
      if ($All) {
        $search = New-Object -Type PSCustomObject -Property @{'criteria'=
          New-Object -Type PSCustomObject -Property @{
          'fields'=@('tags', 'task_id', 'state', 'start_time', 'finish_time')}}
      } else {
        $search = New-Object -Type PSCustomObject -Property @{'criteria'=
          New-Object -Type PSCustomObject -Property @{'fields'=
          @('tags', 'task_id', 'state', 'start_time', 'finish_time');'filters'=
          New-Object -Type PSCustomObject -Property @{'state'=
          New-Object -Type PSCustomObject -Property @{'$in'=
          @('running','waiting')}}}}
        }
      Invoke-PulpRestMethod -Server $Server -Port $Port `
        -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
        -Uri $uri -Method Post -Body (ConvertTo-Json -Depth 20 $search)
    }
  }
  Process {
    If ($TaskId) {
      Foreach ($t in $TaskId) {
        $uri = "/pulp/api/v2/tasks/${t}/"
        $result = Invoke-PulpRestMethod -Server $Server -Port $Port -Uri $uri `
          -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod
        Get-PulpTask -Server $Server -Port $Port -Protocol $Protocol `
          -AuthenticationMethod $AuthenticationMethod $result
      }
    }
    Elseif ($Task) {
      If ($Task.task_id -ne $null) {
        $TaskIds = $Task | Select-Object -ExpandProperty task_id
      }
      Foreach ($t in $TaskIDs) {
        $uri = "/pulp/api/v2/tasks/${t}/"
        Invoke-PulpRestMethod -Server $Server -Port $Port -Uri $uri `
          -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod
      }
      If (($Task.spawned_tasks -ne $null) -and !$NoSpawnedTasks){
        $spawnedTasks = Get-PulpTask -Server $Server -Port $Port -Protocol $Protocol `
          -AuthenticationMethod $AuthenticationMethod `
          ($Task | Select-Object -ExpandProperty spawned_tasks)
        Get-PulpTask -Server $Server -Port $Port -Protocol $Protocol `
          -AuthenticationMethod $AuthenticationMethod $spawnedTasks
      }
    }
  }
}
