---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Copy-PulpPuppetModule.md
schema: 2.0.0
---

# Copy-PulpPuppetModule

## SYNOPSIS
Copy Puppet modules between Pulp Puppet repositories.

## SYNTAX

```
Copy-PulpPuppetModule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -ContentUnit <PSObject[]> [-TargetRepoId] <String[]> [-NoPublish] [<CommonParameters>]
```

## DESCRIPTION
This function copies one or more Puppet modules to one or more target Pulp
Puppet repositories.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpPuppetRepo archive | Get-PulpPuppetModule -Name 'puppetlabs*' | Copy-PulpPuppetModule -TargetRepoId 'my_repo*'
```

Copy all Puppet modules matching `puppetlabs*` from `archive` Puppet repo to all
Puppet repos matching `my_repo*`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -ContentUnit
Content unit objects (Puppet modules) to be copied.

```yaml
Type: PSObject[]
Parameter Sets: (All)
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -TargetRepoId
Target Puppet repo IDs.

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 0
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -NoPublish
Do not publish the target repositories after the operation.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp content unit objects.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp task objects.

## NOTES
NAME:  Copy-PulpPuppetModule

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
