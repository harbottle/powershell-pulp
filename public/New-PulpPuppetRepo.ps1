# .ExternalHelp powershell-pulp-help.xml
Function New-PulpPuppetRepo {
  [CmdletBinding()]
  Param(
    [string]$Server,
    [int]$Port,
    [string]$Protocol,
    [string]$AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [object[]]$Repo,

    [Parameter(Mandatory=$true, Position=0, ParameterSetName="Strings")]
    [string]$Id,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$DisplayName,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Description,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Note,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Feed,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$Validate,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$FeedCaCert,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$VerifyFeedSsl,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$FeedCert,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$FeedKey,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$ProxyHost,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$ProxyPort,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$ProxyUser,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$ProxyPass,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [int]$MaxDownloads,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [int]$MaxSpeed,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$ServeHttp,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$ServeHttps,

    [Parameter(Mandatory=$false)]
    [switch]$NoPublish
  )
  Begin {
    try {
      $outBuffer = $null
      if ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'New-PulpRepo', [System.Management.Automation.CommandTypes]::Function)
      $scriptCmd = {& $wrappedCmd @PSBoundParameters -Type 'puppet'}
      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    catch { throw }
  }
  Process {
    try {
      $steppablePipeline.Process($_)
    }
    catch {
      throw
    }
  }
  End {
    try {
      $steppablePipeline.End()
    }
    catch {
      throw
    }
  }
}
