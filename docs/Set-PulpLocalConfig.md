---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Set-PulpLocalConfig.md
schema: 2.0.0
---

# Set-PulpLocalConfig

## SYNOPSIS
Set the local powershell-pulp configuration.

## SYNTAX

### Object
```
Set-PulpLocalConfig [-File <String>] [-Replace] [-Settings] <Object> [<CommonParameters>]
```

### NoSkip
```
Set-PulpLocalConfig [-File <String>] [-Replace] [-Server <String>] [-Port <String>] [-Protocol <String>]
 [-RpmDownloadUrlBase <String>] [-IsoDownloadUrlBase <String>] [-PuppetDownloadUrlBase <String>]
 [-Username <String>] [-AuthenticationMethod <String>] [-NoSkipCertificateCheck] [-UpdateBasicAuthCredentials]
 [<CommonParameters>]
```

### Skip
```
Set-PulpLocalConfig [-File <String>] [-Replace] [-Server <String>] [-Port <String>] [-Protocol <String>]
 [-RpmDownloadUrlBase <String>] [-IsoDownloadUrlBase <String>] [-PuppetDownloadUrlBase <String>]
 [-Username <String>] [-AuthenticationMethod <String>] [-SkipCertificateCheck] [-UpdateBasicAuthCredentials]
 [<CommonParameters>]
```

### Strings
```
Set-PulpLocalConfig [-File <String>] [-Replace] [-Server <String>] [-Port <String>] [-Protocol <String>]
 [-RpmDownloadUrlBase <String>] [-IsoDownloadUrlBase <String>] [-PuppetDownloadUrlBase <String>]
 [-Username <String>] [-AuthenticationMethod <String>] [-UpdateBasicAuthCredentials] [<CommonParameters>]
```

## DESCRIPTION
This function accepts an object representing configuration settings or individual
configuration settings and writes them to a JSON formatted file
(default location: '~\.pulp\admin.json').

It returns the new configuration.

## EXAMPLES

### Example 1
```
PS C:\> Set-PulpLocalConfig -Server server1.domain
```

Sets the server setting in default config file '~\.pulp\admin.json'.

### Example 2
```
PS C:\> Set-PulpLocalConfig -File '~\my_pulp_admin' -Server server1.domain -Username admin -Replace
```

Sets the server and username settings in config file '~\my_pulp_admin',
replacing all existing settings.

## PARAMETERS

### -File
An alternative configuration file.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: ~\.pulp\admin.json
Accept pipeline input: False
Accept wildcard characters: False
```

### -Replace
Replace all existing configuration with new settings.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Settings
An object containing settings.

```yaml
Type: Object
Parameter Sets: Object
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Server
Set "server" value.

```yaml
Type: String
Parameter Sets: NoSkip, Skip, Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Set "port" value.

```yaml
Type: String
Parameter Sets: NoSkip, Skip, Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Set "protocol" value.

```yaml
Type: String
Parameter Sets: NoSkip, Skip, Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Username
Set "username" value.

```yaml
Type: String
Parameter Sets: NoSkip, Skip, Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Set "authentication method" value.

```yaml
Type: String
Parameter Sets: NoSkip, Skip, Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -UpdateBasicAuthCredentials
Update basic auth username and encrypted password (prompts for new password).

```yaml
Type: SwitchParameter
Parameter Sets: NoSkip, Skip, Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -SkipCertificateCheck
Ignore SSL errors when accessing the Pulp server.

```yaml
Type: SwitchParameter
Parameter Sets: Skip
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -NoSkipCertificateCheck
Do not ignore SSL errors when accessing the Pulp server.

```yaml
Type: SwitchParameter
Parameter Sets: NoSkip
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -IsoDownloadUrlBase
Set "IsoDownloadUrlBase" value. This URL will be used when downloading ISO files from the Pulp server using Export-PulpIso and Export-PulpContentUnit commands.

```yaml
Type: String
Parameter Sets: NoSkip, Skip, Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -PuppetDownloadUrlBase
Set "PuppetDownloadUrlBase" value. This URL will be used when downloading Puppet modules from the Pulp server using Export-PulpPuppetModule and Export-PulpContentUnit commands.

```yaml
Type: String
Parameter Sets: NoSkip, Skip, Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -RpmDownloadUrlBase
Set "RpmDownloadUrlBase" value. This URL will be used when downloading RPM files from the Pulp server using Export-PulpRpm and Export-PulpContentUnit commands.

```yaml
Type: String
Parameter Sets: NoSkip, Skip, Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject
powershell-pulp configuration object.

## OUTPUTS

### System.Management.Automation.PSCustomObject
powershell-pulp configuration object.

## NOTES
NAME: Set-PulpLocalConfig

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
