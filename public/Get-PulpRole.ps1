# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpRole {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [object[]]$User,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$Id = @('*'),

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$DisplayName = '*',

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Description = '*'
  )
  Begin {
    $uri = "/pulp/api/v2/roles/"
    $roles = Invoke-PulpRestMethod -Server $Server -Port $Port `
      -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod -Uri $uri
    $matchedRoles = @()
  }
  Process {
    If ($User){
      Foreach ($u in $User){
        $matchedRoles += ($roles | Where-Object { $_.users -contains $u.login })
      }
    } Else {
      Foreach ($i in $Id) {
        $matchedRoles += ($roles | Where-Object {
          ($_.id -like $i) -and ($_.display_name -like $DisplayName) `
                           -and ($_.description -like $Description)})
      }
    }
  }
  End {
    $matchedRoles | Select-Object * -Unique | Sort-Object id
  }
}
