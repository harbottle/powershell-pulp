# .ExternalHelp powershell-pulp-help.xml
Function Copy-PulpContentUnit {
 [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [PSCustomObject[]]$ContentUnit,

    [Parameter(Mandatory=$true, Position=0)]
    [string[]]$TargetRepoId,

    [Parameter(Mandatory=$false)]
    [switch]$NoPublish,

    [Parameter(Mandatory=$false)]
    [string] $Type
  )
  Begin {
    $reposToPublish = @()
  }
  Process {
    If ($Type) {
      $repos = Get-PulpRepo -Server $server -Port $Port -Protocol $Protocol `
               -AuthenticationMethod $AuthenticationMethod -Id $TargetRepoId `
                -Type $Type
    }
    Else {
      $repos = Get-PulpRepo -Server $server -Port $Port -Protocol $Protocol `
               -AuthenticationMethod $AuthenticationMethod -Id $TargetRepoId
    }
    Foreach ($repo in $repos) {
      $sourceId = $ContentUnit | Select-Object -ExpandProperty repo_id
      $targetId = $repo.id
      $unitId = $ContentUnit | Select-Object -ExpandProperty metadata |
                               Select-Object -ExpandProperty _id
      $unitType = $ContentUnit | Select-Object -ExpandProperty unit_type_id
      If (!$Type -or $Type -eq $unitType -or "${Type}_module" -eq $unitType) {
        $uri = "/pulp/api/v2/repositories/${targetId}/actions/associate/"
        $body = '{"source_repo_id": "' + $sourceId +
             '", "override_config": {}, "criteria": {"type_ids": ["' +
             $unitType + '"], "filters": {"unit": {"_id": "' + $unitId + '"}}}}'
        Invoke-PulpRestMethod -Server $Server -Port $Port `
                              -Protocol $Protocol `
                              -AuthenticationMethod $AuthenticationMethod `
                              -Uri $uri -Method Post -Body $body
        $reposToPublish += $targetId
      }
    }    
  }
  End {
    If (!$NoPublish) {
      $reposToPublish |
      Select-object -Unique |
      Get-PulpRepo -Server $Server -Port $Port -Protocol $Protocol `
                   -AuthenticationMethod $AuthenticationMethod |
      Publish-PulpRepo -Server $Server -Port $Port `
                       -Protocol $Protocol `
                       -AuthenticationMethod $AuthenticationMethod
    }
  }
}
