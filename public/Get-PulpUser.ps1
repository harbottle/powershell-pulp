# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpUser {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [object[]]$Role,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$Login = @('*'),

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Name = '*'
  )
  Begin {
    $uri = "/pulp/api/v2/users/"
    $users = Invoke-PulpRestMethod -Server $Server -Port $Port `
      -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod -Uri $uri
    $matchedUsers = @()
  }
  Process {
    If ($Role){
      Foreach ($r in $Role){
        $matchedUsers += ($users | Where-Object { $_.roles -contains $r.id })
      }
    } Else {
      Foreach ($l in $Login) {
        $matchedUsers += ($users | Where-Object {
          ($_.login -like $l) -and ($_.name -like $Name) })
      }
    }
  }
  End {
    $matchedUsers | Select-Object * -Unique | Sort-Object login
  }
}
