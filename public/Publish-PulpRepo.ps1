# .ExternalHelp powershell-pulp-help.xml
Function Publish-PulpRepo {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Repo,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$Id,

    [Parameter(Mandatory=$false)]
    [switch]$Full,

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {}
  Process {
    If ($Repo) {
      $Id = $Repo | Select-Object -ExpandProperty id
    }
    If ($Type) {
      $repos = Get-PulpRepo -Server $server -Port $Port `
                            -Protocol $Protocol `
                            -AuthenticationMethod $AuthenticationMethod `
                            -Id $Id -Type $Type
    } Else {
      $repos = Get-PulpRepo -Server $server -Port $Port `
                            -Protocol $Protocol `
                            -AuthenticationMethod $AuthenticationMethod `
                            -Id $Id
    }
    Foreach ($repo in $repos) {
      If ($repo.notes.'_repo-type' -eq 'rpm-repo') {$dist = 'yum'}
      Elseif ($repo.notes.'_repo-type' -eq 'puppet-repo') {$dist = 'puppet'}
      Elseif ($repo.notes.'_repo-type' -eq 'iso-repo') {$dist = 'iso'}
      $publishId = $repo.id
      If ($Full) { $override = '{"force_full": true}' }
      Else { $override = 'null' }
      $uri = "/pulp/api/v2/repositories/${publishId}/actions//publish/"
      $body = '{"override_config": '+ $override +', "id": "' + $dist +'_distributor"}'
      Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
                            -AuthenticationMethod $AuthenticationMethod `
                            -Uri $uri -Method Post -Body $body
    }
  } #end Process
}
