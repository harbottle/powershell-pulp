# .ExternalHelp powershell-pulp-help.xml
Function Remove-PulpRepo {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [PSCustomObject[]]$Repo,

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {
    $Type = "${Type}-repo"
  }
  Process {
    Foreach ($delRepo in $Repo) {
      $deleteId = $delRepo.id
      $uri = "/pulp/api/v2/repositories/${deleteId}/"
      if (($Type -eq '-repo') -or ($delRepo.notes.'_repo-type' -eq $Type)) {
       Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
                             -AuthenticationMethod $AuthenticationMethod `
                             -Uri $uri -Method Delete
      }
    }
  }
}
