# .ExternalHelp powershell-pulp-help.xml
Function Invoke-PulpRestMethod {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true)]
    [string]$Uri,

    [Parameter(Mandatory=$false)]
    [string]$Method = 'Get',

    [Parameter(Mandatory=$false)]
    [object]$Body,

    [Parameter(Mandatory=$false)]
    [string]$ContentType,

    [Parameter(Mandatory=$false)]
    [string]$InFile,
    
    [Parameter(Mandatory=$false)]
    [int32]$TimeoutSec
  )
  Begin {
    $sslErr = "If your client computer does not trust the SSL certificate " +
            "presented by server ${server}, please trust this certificate or " +
            "try running: Set-PulpLocalConfig -SkipCertificateCheck"
    $trimmedUri = $Uri.TrimStart("/")
    $newUri = "${Protocol}://${Server}:${Port}/${trimmedUri}"
    foreach ($p in @('Server','Port','Protocol','AuthenticationMethod','Uri')) {
      $PSBoundParameters.Remove($p) | Out-Null
    }
    try {
      $outBuffer = $null
      If ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'Invoke-RestMethod', [System.Management.Automation.CommandTypes]::Cmdlet)
      
      # Turn off certificate checking only if required
      If (($PSVersionTable.PSVersion.Major -lt 6) -and
         ((Get-PulpLocalConfig -SkipCertificateCheck).SkipCertificateCheck)) {
        If (-not ([System.Management.Automation.PSTypeName]'PowershellPulp.SkipCertificateCheck').Type) {
            Add-Type -Path $PSScriptRoot\..\lib\powershell-pulp-skip.cs
            [System.Net.ServicePointManager]::CertificatePolicy =
              New-Object PowershellPulp.SkipCertificateCheck
        }
      }
      
      # Get cert or authentication header
      If ($AuthenticationMethod -eq 'certificate') {
        $cert = Get-PulpCertificate -Server $Server -Port $Port -Protocol $Protocol
      } Else{
        $authHeaders = Get-PulpBasicAuthHeaders -Server $Server -Port $Port -Protocol $Protocol
      }

      #v5 certauth skip
      If (($AuthenticationMethod -eq 'certificate') -and
         ($PSVersionTable.PSVersion.Major -lt 6) -and
         ((Get-PulpLocalConfig -SkipCertificateCheck).SkipCertificateCheck)) {
        $scriptCmd = {& $wrappedCmd @PSBoundParameters -Uri $newUri -Certificate $cert}
      }
      
      #v5 basicauth skip
      If (($AuthenticationMethod -ne 'certificate') -and
         ($PSVersionTable.PSVersion.Major -lt 6) -and
         ((Get-PulpLocalConfig -SkipCertificateCheck).SkipCertificateCheck)) {
        $scriptCmd = {& $wrappedCmd @PSBoundParameters -Uri $newUri -Headers $authHeaders}
      }
      
      #v6 certcauth skip
      If (($AuthenticationMethod -eq 'certificate') -and
         ($PSVersionTable.PSVersion.Major -ge 6) -and
         ((Get-PulpLocalConfig -SkipCertificateCheck).SkipCertificateCheck)) {
        $scriptCmd = {& $wrappedCmd @PSBoundParameters -Uri $newUri -Certificate $cert -SkipCertificateCheck}
      }
      
      #v6 basicauth skip
      If (($AuthenticationMethod -ne 'certificate') -and
         ($PSVersionTable.PSVersion.Major -ge 6) -and
         ((Get-PulpLocalConfig -SkipCertificateCheck).SkipCertificateCheck)) {
        $scriptCmd = {& $wrappedCmd @PSBoundParameters -Uri $newUri -Headers $authHeaders -SkipCertificateCheck}
      }
      
      #v5/v6 certauth noskip
      If (($AuthenticationMethod -eq 'certificate') -and
         !((Get-PulpLocalConfig -SkipCertificateCheck).SkipCertificateCheck)) {
        $scriptCmd = {& $wrappedCmd @PSBoundParameters -Uri $newUri -Certificate $cert}
      }
      
      #v5/v6 basicauth noskip
      If (($AuthenticationMethod -ne 'certificate') -and
         !((Get-PulpLocalConfig -SkipCertificateCheck).SkipCertificateCheck)) {
        $scriptCmd = {& $wrappedCmd @PSBoundParameters -Uri $newUri -Headers $authHeaders}
      }

      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    catch { throw }
  }
  Process {
    try {
      $steppablePipeline.Process($_)
    }
    catch {
      Write-Error $sslErr
      throw
    }
  }
  End {
    try {
      $steppablePipeline.End()
    }
    catch {
      throw
    }
  }
}
