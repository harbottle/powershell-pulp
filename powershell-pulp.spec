%global moduledir /usr/local/share/powershell/Modules

Name:           powershell-pulp
Version:        __VERSION__
Release:        1.harbottle
Summary:        A PowerShell module for Pulp.
Group:          Applications/System
License:        MIT
URL:            https://gitlab.com/harbottle/%{name}
Source0:        https://gitlab.com/harbottle/%{name}/-/archive/v%{version}/%{name}-v%{version}.tar.gz

%description
The module enables the following from the comfort of your PowerShell command
prompt:

- HTTP Basic Auth: Authenticate to Pulp using username and password and securely
  store your Pulp password for later commands (HTTP basic auth).
- Download and store your user Pulp certificate in your Windows certificate
  store (or user-cert.pem file on other platforms). This allows you to identify
  yourself with your Pulp-issued certificate and issue further commands without
  specifiying your username and password each time (Certificate auth).
- Create, modify, publish, synchronize and remove Pulp repos.
- Set repo synchronization schedules and start syncs on-demand.
- Upload, export (download), copy and delete content units of various types
  (including RPMs, ISOs and Puppet modules).
- Remove orphaned content.
- Manage Pulp users, roles and permissions
- Manage Pulp tasks.
- List and remove orphaned content units.
- Run arbitrary commands against the Pulp REST interface.

%prep
%setup -q -n %{name}-v%{version}

%install
mkdir -p $RPM_BUILD_ROOT%{moduledir}/%{name}/%{version}
mv * $RPM_BUILD_ROOT%{moduledir}/%{name}/%{version}

%files
%defattr(-,root,root,-)
%{moduledir}/%{name}/%{version}

%changelog
* Mon Apr 30 2018 - Richard Grainger <grainger@gmail.com> - 3.0.4-1
- Initial RPM packaging, see git log for other changes
