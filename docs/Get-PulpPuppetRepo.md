---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpPuppetRepo.md
schema: 2.0.0
---

# Get-PulpPuppetRepo

## SYNOPSIS
Get Pulp Puppet repositories.

## SYNTAX

```
Get-PulpPuppetRepo [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-Id] <String[]>] [<CommonParameters>]
```

## DESCRIPTION
This function gets Pulp Puppet repositories based on the repo ID provided.
Wildcards may be used. If no ID is provided, all Puppet repos will be returned.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpPuppetRepo
```

Get all Pulp Puppet repos.

### Example 2
```
PS C:\> Get-PulpPuppetRepo -Id modules
```

Get the `modules` Pulp Puppet repo.

### Example 3
```
PS C:\> Get-PulpPuppetRepo -Id 'forge*'
```

Get all Pulp repos with ID matching `forge*`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Id
Pulp repo IDs.

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: False
Position: 0
Default value: *
Accept pipeline input: True (ByValue)
Accept wildcard characters: True
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

## NOTES
NAME:  Get-PulpPuppetRepo

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
