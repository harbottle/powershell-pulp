# .ExternalHelp powershell-pulp-help.xml
Function Add-PulpRpm {
  [CmdletBinding(DefaultParameterSetName='StringsPath')]
  Param(
    [string]$Server,
    [int]$Port,
    [string]$Protocol,
    [string]$AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="ObjectsPath")]
    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="ObjectsUrl")]
    [PSCustomObject[]]$TargetRepo,

    [Parameter(Mandatory=$true, Position=1, ValueFromPipeline=$true, ParameterSetName="StringsPath")]
    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="StringsUrl")]
    [string[]]$TargetRepoId,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$false, ParameterSetName="ObjectsPath")]
    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$false, ParameterSetName="StringsPath")]
    [string[]]$Path,

    [Parameter(Mandatory=$true, ValueFromPipeline=$false, ParameterSetName="ObjectsUrl")]
    [Parameter(Mandatory=$true, ValueFromPipeline=$false, ParameterSetName="StringsUrl")]
    [string]$Url,

    [Parameter(Mandatory=$false)]
    [switch]$NoPublish
  )
  Begin {
    try {
      $outBuffer = $null
      if ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'Add-PulpContentUnit', [System.Management.Automation.CommandTypes]::Function)
      $scriptCmd = {& $wrappedCmd @PSBoundParameters -Type 'rpm'}
      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    catch { throw }
  }
  Process {
    try {
      $steppablePipeline.Process($_)
    }
    catch {
      throw
    }
  }
  End {
    try {
      $steppablePipeline.End()
    }
    catch {
      throw
    }
  }
}
