---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Add-PulpRoleMember.md
schema: 2.0.0
---

# Add-PulpRoleMember

## SYNOPSIS
Add Pulp users to Pulp roles.

## SYNTAX

### UserStringsRoleStrings (Default)
```
Add-PulpRoleMember [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-UserLogin] <String[]> -RoleId <String[]> [<CommonParameters>]
```

### UserObjectsRoleStrings
```
Add-PulpRoleMember [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-User] <PSObject[]> -RoleId <String[]> [<CommonParameters>]
```

### UserObjectsRoleObjects
```
Add-PulpRoleMember [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-User] <PSObject[]> -Role <PSObject[]> [<CommonParameters>]
```

### UserStringsRoleObjects
```
Add-PulpRoleMember [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -Role <PSObject[]> [-UserLogin] <String[]> [<CommonParameters>]
```

## DESCRIPTION
This function adds one or more Pulp users to one or more Pulp roles.

- At least one user object or login must be supplied.
- At least one role object or role ID must be supplied.

## EXAMPLES

### Example 1
```
Get-PulpRole editors | Add-PulpRoleMember -UserLogin alice
```

Add user alice to role "editors".

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -RoleId
Pulp role IDs.

```yaml
Type: String[]
Parameter Sets: UserStringsRoleStrings, UserObjectsRoleStrings
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Role
Pulp role objects.

```yaml
Type: PSObject[]
Parameter Sets: UserObjectsRoleObjects, UserStringsRoleObjects
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -UserLogin
Pulp user logins.

```yaml
Type: String[]
Parameter Sets: UserStringsRoleStrings, UserStringsRoleObjects
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -User
Pulp user objects.

```yaml
Type: PSObject[]
Parameter Sets: UserObjectsRoleStrings, UserObjectsRoleObjects
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp role objects.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp role objects.

### None

## NOTES
NAME:  Add-PulpRoleMember

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
