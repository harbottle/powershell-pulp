---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Add-PulpRpm.md
schema: 2.0.0
---

# Add-PulpRpm

## SYNOPSIS
Upload files to Pulp RPM repositories.

## SYNTAX

### StringsPath (Default)
```
Add-PulpRpm [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepoId <String[]> [-Path] <String[]> [-NoPublish] [<CommonParameters>]
```

### ObjectsUrl
```
Add-PulpRpm [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepo <PSObject[]> -Url <String> [-NoPublish] [<CommonParameters>]
```

### ObjectsPath
```
Add-PulpRpm [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepo <PSObject[]> [-Path] <String[]> [-NoPublish] [<CommonParameters>]
```

### StringsUrl
```
Add-PulpRpm [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepoId <String[]> -Url <String> [-NoPublish] [<CommonParameters>]
```

## DESCRIPTION
This function uploads one or more files to one or more RPM repositories on a
Pulp server.

One of the following must be supplied:

- A file path pattern for the source files to be uploaded.
- A URL for the source file to be uploaded.

One of the following must also be supplied as an argument or using the pipeline:

- Target repo IDs.
- Target repo objects

If multiple repos are supplied in any way (wildcards, arrays or pipeline), files
will be uploaded to the first repo and then copied from that repo to the other
specified repos. This is to ensure the same file is not uploaded multiple times.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpRpmRepo my_rpms* | Add-PulpRpm C:\RPMs\*.rpm
```

Upload all RPM files in `C:\RPMs` to all RPM repos matching ID `my_rpms*`.

### Example 2
```
PS C:\> Add-PulpRpm -Url 'https://foo.com/bar.rpm' -TargetRepoId rpm_archive
```

Download https://foo.com/bar.rpm and upload it to RPM repo with ID
`rpm_archive`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -TargetRepo
Target repo objects.

```yaml
Type: PSObject[]
Parameter Sets: ObjectsUrl, ObjectsPath
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -TargetRepoId
Target repo IDs.

```yaml
Type: String[]
Parameter Sets: StringsPath, StringsUrl
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Path
Path of the files to be added.

```yaml
Type: String[]
Parameter Sets: StringsPath, ObjectsPath
Aliases:

Required: True
Position: 0
Default value: None
Accept pipeline input: False
Accept wildcard characters: True
```

### -Url
URL of the file to be added.

```yaml
Type: String
Parameter Sets: ObjectsUrl, StringsUrl
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -NoPublish
Do not publish the target repositories after the operation.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp task objects.

## NOTES
NAME:  Add-PulpRpm

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
