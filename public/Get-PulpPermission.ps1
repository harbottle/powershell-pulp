# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpPermission {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Resource,

    [Parameter(Mandatory=$false,Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [String[]]$Href,

    [Parameter(Mandatory=$false,ParameterSetName="Objects")]
    [string]$StripUriPrefix = '/pulp/api'
  )
  Begin {}
  Process {
    If ($Resource) {
     $Href = $Resource | Select-Object -ExpandProperty '_href'
     $Href = ($Href | ForEach-Object {$_ -replace "^${StripUriPrefix}",'' })
    }
    If ($Href) {
      foreach ($item in $Href) {
        $encodedHref = [System.Web.HttpUtility]::UrlEncode($href)
        $uri = "/pulp/api/v2/permissions/?resource=${encodedHref}"
        Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
          -AuthenticationMethod $AuthenticationMethod -Uri $uri
      }
    }
    Else {
      $uri = "/pulp/api/v2/permissions/"
      Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Uri $uri
    }
  }
}
