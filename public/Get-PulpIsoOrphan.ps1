# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpIsoOrphan {
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string] $Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true)]
    [string[]]$Name = '*'
  )
  Begin {
    Try {
      $outBuffer = $null
      If ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'Get-PulpOrphan',
        [System.Management.Automation.CommandTypes]::Function)
      $scriptCmd = {& $wrappedCmd @PSBoundParameters -Type 'iso' }
      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    Catch {
      Throw
    }
  }
  Process {
    Try {
      $steppablePipeline.Process($_)
    }
    Catch {
      Throw
    }
  }
  End {
    Try {
      $steppablePipeline.End()
    }
    Catch {
      Throw
    }
  }
}
