---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpUser.md
schema: 2.0.0
---

# Get-PulpUser

## SYNOPSIS
Get pulp users..

## SYNTAX

### Strings (Default)
```
Get-PulpUser [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-Login] <String[]>] [[-Name] <String>] [<CommonParameters>]
```

### Objects
```
Get-PulpUser [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -Role <Object[]> [<CommonParameters>]
```

## DESCRIPTION
This function returns Pulp users. 

The following may be supplied:

- One or more user logins (wildcards accepted).
- One or more Pulp roles objects.  Users contained in these roles will be returned.

Users can also be filtered by user name.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpRole developers | Get-PulpUser
```

Get all users in developers  role.

### Example 2
```
PS C:\> Get-PulpUser -Login grace
```

Get user grace.

### Example 3
```
PS C:\> Get-PulpUser -Name 'Grace*'
```

Get all users with name matching `Grace*`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Login
User logins.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: False
Position: 1
Default value: @('*')
Accept pipeline input: True (ByValue)
Accept wildcard characters: True
```

### -Role
Pulp role objects.

```yaml
Type: Object[]
Parameter Sets: Objects
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Name
Name of the user.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: 2
Default value: *
Accept pipeline input: False
Accept wildcard characters: True
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp role objects.

### System.String[]
Pulp user logins.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp user objects.

## NOTES
NAME:  Get-PulpUser

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
