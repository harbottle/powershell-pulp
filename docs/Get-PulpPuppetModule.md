---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpPuppetModule.md
schema: 2.0.0
---

# Get-PulpPuppetModule

## SYNOPSIS
Get Pulp Puppet modules.

## SYNTAX

### Strings (Default)
```
Get-PulpPuppetModule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-Name] <String>] -RepoId <String[]> [<CommonParameters>]
```

### Objects
```
Get-PulpPuppetModule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-Name] <String>] -Repo <PSObject[]> [<CommonParameters>]
```

## DESCRIPTION
This function gets Pulp Puppet module content unit objects from the specified
Puppet source repos. At least one Puppet source repo object or ID must be
supplied.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpPuppetRepo archive | Get-PulpPuppetModule -Name 'puppetlabs*'
```

Get all Puppet modules matching `puppetlabs*` from `archive` Puppet repo.

### Example 2
```
PS C:\> Get-PulpPuppetModule -RepoId archive
```

Get all Puppet modules from `archive` Puppet repo.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -RepoId
Source repo IDs.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Repo
Source repo objects.

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Name
Content unit name.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 0
Default value: None
Accept pipeline input: False
Accept wildcard characters: True
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp content unit objects.

## NOTES
NAME:  Get-PulpPuppetModule

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
