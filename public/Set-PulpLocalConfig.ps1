# .ExternalHelp powershell-pulp-help.xml
Function Set-PulpLocalConfig {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$File = '~\.pulp\admin.json',

    [Parameter(Mandatory=$false)]
    [switch]$Replace,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Object")]
    [object]$Settings,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [Parameter(Mandatory=$false, ParameterSetName="Skip")]
    [Parameter(Mandatory=$false, ParameterSetName="NoSkip")]
    [string]$Server,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [Parameter(Mandatory=$false, ParameterSetName="Skip")]
    [Parameter(Mandatory=$false, ParameterSetName="NoSkip")]
    [string]$Port,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [Parameter(Mandatory=$false, ParameterSetName="Skip")]
    [Parameter(Mandatory=$false, ParameterSetName="NoSkip")]
    [string]$Protocol,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [Parameter(Mandatory=$false, ParameterSetName="Skip")]
    [Parameter(Mandatory=$false, ParameterSetName="NoSkip")]
    [string]$RpmDownloadUrlBase,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [Parameter(Mandatory=$false, ParameterSetName="Skip")]
    [Parameter(Mandatory=$false, ParameterSetName="NoSkip")]
    [string]$IsoDownloadUrlBase,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [Parameter(Mandatory=$false, ParameterSetName="Skip")]
    [Parameter(Mandatory=$false, ParameterSetName="NoSkip")]
    [string]$PuppetDownloadUrlBase,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [Parameter(Mandatory=$false, ParameterSetName="Skip")]
    [Parameter(Mandatory=$false, ParameterSetName="NoSkip")]
    [string]$Username,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [Parameter(Mandatory=$false, ParameterSetName="Skip")]
    [Parameter(Mandatory=$false, ParameterSetName="NoSkip")]
    [string]$AuthenticationMethod,

    [Parameter(Mandatory=$true, ParameterSetName="Skip")]
    [switch]$SkipCertificateCheck,

    [Parameter(Mandatory=$true, ParameterSetName="NoSkip")]
    [switch]$NoSkipCertificateCheck,

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [Parameter(Mandatory=$false, ParameterSetName="Skip")]
    [Parameter(Mandatory=$false, ParameterSetName="NoSkip")]
    [switch]$UpdateBasicAuthCredentials
  )
  If ($Settings){
    $newSettings = New-Object -TypeName PSCustomObject
      if (!$Replace){
      $readSettings = Get-PulpLocalConfig -File $File
      $readSettings.PSObject.Properties |
      foreach-object {
        Add-Member -InputObject $newSettings -MemberType NoteProperty `
                    -Name $_.Name -Value $_.Value
      }
    }
    $Settings.PSObject.Properties |
    foreach-object {
      $setting = $_.Name
      $value = $_.Value
      Add-Member -InputObject $newSettings -MemberType NoteProperty `
                 -Name $setting -Value $value -Force

    }
    $null = (New-Item -ItemType Directory -Force -Path (Split-Path $File))
    $newSettings | ConvertTo-JSON | Out-File $File
    Get-PulpLocalConfig -File $File
  }
  else {
    $params = $PSBoundParameters.GetEnumerator()|
              Where-Object {($_.Key -ne 'File' -and
                             $_.Key -ne 'Settings' -and
                             $_.Key -ne 'Replace')}
    $Settings = New-Object -TypeName PSCustomObject
    foreach ($parameter in $params){
      $setting = $parameter.Key.ToLower()
      $value = $parameter.Value
        if ($value){
          if ($setting -eq 'UpdateBasicAuthCredentials') {
            $credential = Get-Credential -User ((Get-PulpLocalConfig -Username).Username) `
                                         -Message "Enter Pulp HTTP Basic Auth credentials to store securely"
            Add-Member -InputObject $Settings -MemberType NoteProperty `
                       -Name 'username' -Value $credential.Username
            Add-Member -InputObject $Settings -MemberType NoteProperty `
                       -Name 'password' `
                       -Value ($credential.Password | ConvertFrom-SecureString)
          }
          Elseif ($setting -eq 'skipcertificatecheck') {
            Add-Member -InputObject $Settings -MemberType NoteProperty `
                       -Name 'skipcertificatecheck' -Value $true
          }
          Elseif ($setting -eq 'noskipcertificatecheck') {
            Add-Member -InputObject $Settings -MemberType NoteProperty `
                       -Name 'skipcertificatecheck' -Value $false
          }
          Else {
            Add-Member -InputObject $Settings -MemberType NoteProperty `
                       -Name $setting -Value $value
          }
        }
    }
    Set-PulpLocalConfig -File $File -Settings $Settings -Replace:$Replace
  }
}
