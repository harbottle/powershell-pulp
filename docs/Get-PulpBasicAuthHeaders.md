---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpBasicAuthHeaders.md
schema: 2.0.0
---

# Get-PulpBasicAuthHeaders

## SYNOPSIS
Get HTTP basic authentication headers.

## SYNTAX

```
Get-PulpBasicAuthHeaders [-Server <String>] [-Port <Int32>] [-Protocol <String>] [<CommonParameters>]
```

## DESCRIPTION
This function gets HTTP basic authentication headers containing suitable for
Pulp authentication.  If a username and password are not saved, the user is
prompted.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpBasicAuthHeaders
```

Get basic auth headers for default server.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### None

## OUTPUTS

### System.Object
HTTP basic auth headers object.

## NOTES
NAME:  Get-PulpBasicAuthHeaders

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
