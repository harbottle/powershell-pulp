---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Start-PulpSync.md
schema: 2.0.0
---

# Start-PulpSync

## SYNOPSIS
Start Pulp repo sync.

## SYNTAX

### Strings (Default)
```
Start-PulpSync [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -RepoId <String[]> [-Type <String>] [<CommonParameters>]
```

### Objects
```
Start-PulpSync [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Repo] <PSObject[]> [-Type <String>] [<CommonParameters>]
```

## DESCRIPTION
This function starts synchronization for specified Pulp repositories with a
configured upstream feed. Repos must be specified using either objects or IDs.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpRepo archive | Start-PulpSync
```

Starts synchronization of repo `archive` with its configured upstream feed.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Type
Repo type.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -RepoId
Pulp repo IDs.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Repo
Pulp repo objects.

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: True
Position: 0
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp task objects.

## NOTES
NAME:  Start-PulpSync

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
