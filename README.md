# powershell-pulp

[![logo](logo128.png)](https://gitlab.com/harbottle/powershell-pulp/tags)
[![Download](https://img.shields.io/badge/Download-powershell--pulp-green.svg)](https://gitlab.com/harbottle/powershell-pulp/-/tags)
[![PowerShell Gallery - version](https://img.shields.io/powershellgallery/v/powershell-pulp.svg)](https://www.powershellgallery.com/packages/powershell-pulp)
[![PowerShell Gallery - downloads](https://img.shields.io/powershellgallery/dt/powershell-pulp.svg)](https://www.powershellgallery.com/packages/powershell-pulp)
[![Docker - pulls](https://img.shields.io/docker/pulls/harbottle/powershell-pulp.svg)](https://hub.docker.com/r/harbottle/powershell-pulp)
[![GitLab - build status](https://gitlab.com/harbottle/powershell-pulp/badges/master/pipeline.svg)](https://gitlab.com/harbottle/powershell-pulp/pipelines)

A PowerShell module for [Pulp](http://www.pulpproject.org/).

**NEWS:** Version 3 of the module is now compatible with PowerShell Core on
every platform (certain platforms may require PowerShell Core version
`6.1.0~preview.4` or above). Windows PowerShell compatibility has also been
maintained.

## Intro

The module enables the following from the comfort of your PowerShell command
prompt:

 - Authenticate to Pulp using username and password and securely store your
   Pulp password for later commands (HTTP basic auth).
 - Download and store your user Pulp certificate in your Windows
   certificate store (or `user-cert.pem` file on other platforms). This allows
   you to identify yourself with your Pulp-issued certificate and issue further
   commands without specifiying your username and password each time (Certificate
   auth).
 - Create, modify, publish, synchronize and remove Pulp repos.
 - Set repo synchronization schedules and start syncs on-demand.
 - Upload, export (download), copy and delete content units of various types
  (including RPMs, ISOs and Puppet modules).
 - Remove orphaned content.
 - Manage Pulp users, roles and permissions
 - Manage Pulp tasks.
 - List and remove orphaned content units.
 - Run arbitrary commands against the Pulp REST interface.

PowerShell's superpower is the ability to pipe objects between commands and
powershell-pulp leverages this capacity.

Examples:

```powershell
# Copy all "wildfly" RPMs from all repos matching wildcard "centos*" to "archive" repo
Get-PulpRpmRepo 'centos*' | Get-PulpRpm 'wildfly*' | Copy-PulpRpm archive
```

```powershell
# Upload all `tar.gz` files in the current directory to the "tarballs" iso-type repo
Get-PulpIsoRepo tarballs | Add-PulpIso ./*.tar.gz
```

```powershell
# Remove all Puppet modules from the "puppetmodules" repo
Get-PulpPuppetRepo puppetmodules | Get-PulpPuppetModule | Remove-PulpPuppetModule
```

```powershell
# Add a module from the public Puppet Forge to all the local Puppet repos
Get-PulpPuppetRepo | Add-PulpPuppetModule -Author puppetlabs -Name stdlib -Version 4.25.1
```

```powershell
# Start a sync from upstream for all local CentOS RPM repos
Get-PulpRpmRepo 'centos*' | Start-PulpRpmSync
```

## Install

There are various ways to install this module:

### PowerShellGet

Install powershell-pulp from the
[PowerShell Gallery](https://www.powershellgallery.com/packages/powershell-pulp)
using
[PowerShellGet](https://msdn.microsoft.com/powershell/reference/5.1/PowerShellGet/PowerShellGet).
PowerShellGet is included in Windows PowerShell 5 and also in PowerShell Core on
all platforms.

To install for the current user:

```powershell
   Install-Module -Name powershell-pulp -Scope CurrentUser
```

To install for all users (requires admin elevation on Windows or root access on
other platforms):

```powershell
   Install-Module -Name powershell-pulp -Scope AllUsers
```

### Windows MSI

[Download](https://gitlab.com/harbottle/powershell-pulp/tags) and install the
latest powershell-pulp MSI package (admin elevation required). This will
enable the module for all computer users in both Windows PowerShell and PowerShell
Core.

### RPM

For RPM-based Linux distributions (RHEL, CentOS, Fedora, etc.), you can
[Download](https://gitlab.com/harbottle/powershell-pulp/tags) and install the
latest powershell-pulp RPM package (root permissions required). This will
enable the module for all computer users.

### Yum

RHEL and CentOS 7 also have the option of using the
[harbottle-main Yum repo](https://gitlab.com/harbottle/harbottle-main) to
install the powershell-pulp RPM:

```bash
yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
yum -y install powershell-pulp
```

### DEB

For Debian-based Linux distributions (Debian, Ubuntu, etc.), you can
[Download](https://gitlab.com/harbottle/powershell-pulp/tags) and install the
latest powershell-pulp DEB package (root permissions required). This will
enable the module for all computer users.

### Docker

To try the Linux-based Docker image, run:

```bash
docker run -it harbottle/powershell-pulp
```

## Getting started
After installing, the module should autoload the next time you
start PowerShell. To enable the module manually, type the following:

````powershell
Import-Module powershell-pulp
````

If your Pulp server uses username and password authentication (HTTP Basic auth),
try this:

```powershell
# Edit as appropriate
Set-PulpLocalConfig -Server server.domain -AuthenticationMethod basic -UpdateBasicAuthCredentials -SkipCertificateCheck

# Now test, e.g.
Get-PulpRepo
```

If your Pulp server uses certificate authentication, try this:

```powershell
# Edit as appropriate
Set-PulpLocalConfig -Server server.domain -AuthenticationMethod certificate -Username your_username -SkipCertificateCheck
Get-PulpCertificate

# Now test, e.g.
Get-PulpRepo
```

(the `-SkipCertificateCheck` flag is only required if your client does not
already trust the CA certificate presented by the Pulp server, as is often the
case with self-signed certs)

## Requirements
1. [Windows PowerShell 3](http://www.microsoft.com/en-us/download/details.aspx?id=34595)
and above or [PowerShell Core](https://github.com/powershell/powershell)
2. A [Pulp](http://www.pulpproject.org/) server.

## Getting help
Use Get-Help, e.g.:

````powershell
Get-Help Get-PulpLocalConfig -Online
````

## Functions

### Config

- [Get-PulpLocalConfig](docs/Get-PulpLocalConfig.md)
- [Get-PulpBasicAuthHeaders](docs/Get-PulpBasicAuthHeaders.md)
- [Get-PulpCertificate](docs/Get-PulpCertificate.md)
- [Set-PulpLocalConfig](docs/Set-PulpLocalConfig.md)

### Repos

- [Get-PulpRepo](docs/Get-PulpRepo.md)
- [Get-PulpIsoRepo](docs/Get-PulpIsoRepo.md)
- [Get-PulpPuppetRepo](docs/Get-PulpPuppetRepo.md)
- [Get-PulpRpmRepo](docs/Get-PulpRpmRepo.md)

- [New-PulpRepo](docs/New-PulpRepo.md)
- [New-PulpIsoRepo](docs/New-PulpIsoRepo.md)
- [New-PulpPuppetRepo](docs/New-PulpPuppetRepo.md)
- [New-PulpRpmRepo](docs/New-PulpRpmRepo.md)

- [Publish-PulpRepo](docs/Publish-PulpRepo.md)
- [Publish-PulpRpmRepo](docs/Publish-PulpRpmRepo.md)
- [Publish-PulpIsoRepo](docs/Publish-PulpIsoRepo.md)
- [Publish-PulpPuppetRepo](docs/Publish-PulpPuppetRepo.md)

- [Remove-PulpRepo](docs/Remove-PulpRepo.md)
- [Remove-PulpIsoRepo](docs/Remove-PulpIsoRepo.md)
- [Remove-PulpPuppetRepo](docs/Remove-PulpPuppetRepo.md)
- [Remove-PulpRpmRepo](docs/Remove-PulpRpmRepo.md)

- [Set-PulpRepo](docs/Set-PulpRepo.md)
- [Set-PulpIsoRepo](docs/Set-PulpIsoRepo.md)
- [Set-PulpPuppetRepo](docs/Set-PulpPuppetRepo.md)
- [Set-PulpRpmRepo](docs/Set-PulpRpmRepo.md)

### Content

- [Add-PulpContentUnit](docs/Add-PulpContentUnit.md)
- [Add-PulpIso](docs/Add-PulpIso.md)
- [Add-PulpPuppetModule](docs/Add-PulpPuppetModule.md)
- [Add-PulpRpm](docs/Add-PulpRpm.md)

- [Copy-PulpContentUnit](docs/Copy-PulpContentUnit.md)
- [Copy-PulpIso](docs/Copy-PulpIso.md)
- [Copy-PulpPuppetModule](docs/Copy-PulpPuppetModule.md)
- [Copy-PulpRpm](docs/Copy-PulpRpm.md)

- [Export-PulpContentUnit](docs/Export-PulpContentUnit.md)
- [Export-PulpIso](docs/Export-PulpIso.md)
- [Export-PulpPuppetModule](docs/Export-PulpPuppetModule.md)
- [Export-PulpRpm](docs/Copy-ExportRpm.md)

- [Get-PulpContentUnit](docs/Get-PulpContentUnit.md)
- [Get-PulpIso](docs/Get-PulpIso.md)
- [Get-PulpPuppetModule](docs/Get-PulpPuppetModule.md)
- [Get-PulpRpm](docs/Get-PulpRpm.md)

- [Remove-PulpContentUnit](docs/Remove-PulpContentUnit.md)
- [Remove-PulpIso](docs/Remove-PulpIso.md)
- [Remove-PulpPuppetModule](docs/Remove-PulpPuppetModule.md)
- [Remove-PulpRpm](docs/Remove-PulpRpm.md)

### Sync

- [Get-PulpSyncSchedule](docs/Get-PulpSyncSchedule.md)
- [Get-PulpIsoSyncSchedule](docs/Get-PulpIsoSyncSchedule.md)
- [Get-PulpPuppetSyncSchedule](docs/Get-PulpPuppetSyncSchedule.md)
- [Get-PulpRpmSyncSchedule](docs/Get-PulpRpmSyncSchedule.md)

- [Get-PulpSyncStatus](docs/Get-PulpSyncStatus.md)
- [Get-PulpIsoSyncStatus](docs/Get-PulpIsoSyncStatus.md)
- [Get-PulpPuppetSyncStatus](docs/Get-PulpPuppetSyncStatus.md)
- [Get-PulpRpmSyncStatus](docs/Get-PulpRpmSyncStatus.md)

- [New-PulpSyncSchedule](docs/New-PulpSyncSchedule.md)
- [New-PulpIsoSyncSchedule](docs/New-PulpIsoSyncSchedule.md)
- [New-PulpPuppetSyncSchedule](docs/New-PulpPuppetSyncSchedule.md)
- [New-PulpRpmSyncSchedule](docs/New-PulpRpmSyncSchedule.md)

- [Remove-PulpSyncSchedule](docs/Remove-PulpSyncSchedule.md)
- [Remove-PulpIsoSyncSchedule](docs/Remove-PulpIsoSyncSchedule.md)
- [Remove-PulpPuppetSyncSchedule](docs/Remove-PulpPuppetSyncSchedule.md)
- [Remove-PulpRpmSyncSchedule](docs/Remove-PulpRpmSyncSchedule.md)

- [Start-PulpSync](docs/Start-PulpSync.md)
- [Start-PulpIsoSync](docs/Start-PulpIsoSync.md)
- [Start-PulpPuppetSync](docs/Start-PulpPuppetSync.md)
- [Start-PulpRpmSync](docs/Start-PulpRpmSync.md)

## Orphans

- [Get-PulpOrphan](docs/Get-PulpOrphan.md)
- [Get-PulpIsoOrphan](docs/Get-PulpIsoOrphan.md)
- [Get-PulpPuppetOrphan](docs/Get-PulpPuppetOrphan.md)
- [Get-PulpRpmOrphan](docs/Get-PulpRpmOrphan.md)

- [Remove-PulpOrphan](docs/Remove-PulpOrphan.md)
- [Remove-PulpIsoOrphan](docs/Remove-PulpIsoOrphan.md)
- [Remove-PulpPuppetOrphan](docs/Remove-PulpPuppetOrphan.md)
- [Remove-PulpRpmOrphan](docs/Remove-PulpRpmOrphan.md)

### Tasks

- [Get-PulpTask](docs/Get-PulpTask.md)
- [Stop-PulpTask](docs/Stop-PulpTask.md)

### Users
- [Get-PulpUser](docs/Get-PulpUser.md)
- [New-PulpUser](docs/New-PulpUser.md)
- [Remove-PulpUser](docs/Remove-PulpUser.md)
- [Set-PulpUser](docs/Set-PulpUser.md)

### Roles

- [Add-PulpRoleMember](docs/Add-PulpRoleMember.md)
- [Get-PulpRole](docs/Get-PulpRole.md)
- [New-PulpRole](docs/New-PulpRole.md)
- [Remove-PulpRole](docs/Remove-PulpRole.md)
- [Remove-PulpRoleMember](docs/Remove-PulpRoleMember.md)
- [Set-PulpRole](docs/Set-PulpRole.md)

### Permissions

- [Get-PulpPermission](docs/Get-PulpPermission.md)
- [Set-PulpPermission](docs/Set-PulpPermission.md)

### Misc

- [Invoke-PulpRestMethod](docs/Invoke-PulpRestMethod.md)

## License and copyright
**powershell-pulp**: Copyright (c) 2015-2019 Richard Grainger,
[MIT License](https://opensource.org/licenses/MIT)

**crypto libraries**: Copyright (c) 2011 Mario Majčica,
[CPOL Licence](http://www.codeproject.com/info/cpol10.aspx) and
Copyright (c) 2018 Československá obchodní banka,
[GPL v3 License](https://www.gnu.org/licenses/gpl-3.0.txt)
