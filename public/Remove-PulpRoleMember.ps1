# .ExternalHelp powershell-pulp-help.xml
Function Remove-PulpRoleMember {
  [Cmdletbinding(DefaultParameterSetName='UserStringsRoleStrings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=1, ParameterSetName="UserObjectsRoleObjects")]
    [Parameter(Mandatory=$true, Position=1, ParameterSetName="UserObjectsRoleStrings")]
    [PSCustomObject[]]$User,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="UserObjectsRoleObjects")]
    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="UserStringsRoleObjects")]
    [PSCustomObject[]]$Role,

    [Parameter(Mandatory=$true, Position=1, ParameterSetName="UserStringsRoleObjects")]
    [Parameter(Mandatory=$true, Position=1, ParameterSetName="UserStringsRoleStrings")]
    [string[]]$UserLogin,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="UserObjectsRoleStrings")]
    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="UserStringsRoleStrings")]
    [string[]]$RoleId
  )
  Begin {
    $userLogins = @()
    If ($User) { $UserLogins = $User | Select-Object -ExpandProperty 'login' }
    If ($UserLogin) { $userLogins += $UserLogin }
  }
  Process {
    If ($Role) { $RoleId = $Role | Select-Object -ExpandProperty 'id' }
    Foreach ($r in $RoleId) {
      Foreach ($u in $userLogins) {
        $uri = "/pulp/api/v2/roles/${r}/users/${u}"
        $null = Invoke-PulpRestMethod -Server $Server -Port $Port `
          -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
          -Uri $uri -Method Delete
      }
      Get-PulpRole -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Id $r
    }
  }
}
