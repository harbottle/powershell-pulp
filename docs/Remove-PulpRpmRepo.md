---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Remove-PulpRpmRepo.md
schema: 2.0.0
---

# Remove-PulpRpmRepo

## SYNOPSIS
Remove Pulp RPM repos.

## SYNTAX

```
Remove-PulpRpmRepo [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Repo] <PSObject[]> [<CommonParameters>]
```

## DESCRIPTION
This function removes Pulp RPM repositories. Pulp RPM repo objects must be
supplied.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpRpmRepo internal | Remove-PulpRpmRepo
```

Remove Pulp RPM repo `internal`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Repo
Pulp repo objects.

```yaml
Type: PSObject[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 4
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

## OUTPUTS

### None

## NOTES
NAME:  Remove-PulpRpmRepo

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
