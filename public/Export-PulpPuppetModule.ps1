# .ExternalHelp powershell-pulp-help.xml
Function Export-PulpPuppetModule{
  [Cmdletbinding()]
  Param(
    [string]$Server,
    [int]$Port,
    [string]$Protocol,
    [string]$AuthenticationMethod,

    [Parameter(Mandatory=$false)]
    [string]$DownloadPath = (Get-Location).Path,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [PSCustomObject[]]$ContentUnit
  )
  Begin {
    Try {
      $outBuffer = $null
      If ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'Export-PulpContentUnit',
        [System.Management.Automation.CommandTypes]::Function)
      $scriptCmd = {& $wrappedCmd @PSBoundParameters -Type 'puppet' }
      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    Catch {
      Throw
    }
  }
  Process {
    Try {
      $steppablePipeline.Process($_)
    }
    Catch {
      Throw
    }
  }
  End {
    Try {
      $steppablePipeline.End()
    }
    Catch {
      Throw
    }
  }
}
