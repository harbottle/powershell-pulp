# .ExternalHelp powershell-pulp-help.xml
Function Set-PulpPermission {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Resource,

    [Parameter(Mandatory=$false,Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [String[]]$Href,

    [Parameter(Mandatory=$false,ParameterSetName="Objects")]
    [string]$StripUriPrefix = '/pulp/api',

    [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
    [PSCustomObject[]]$User,

    [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
    [PSCustomObject[]]$Role,

    [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
    [string[]]$UserLogin,

    [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
    [string[]]$RoleId,

    [Parameter(Mandatory=$false)][switch]$Grant,
    [Parameter(Mandatory=$false)][switch]$Revoke,
    [Parameter(Mandatory=$false)][string]$Permission,
    [Parameter(Mandatory=$false)][switch]$Create,
    [Parameter(Mandatory=$false)][switch]$Read,
    [Parameter(Mandatory=$false)][switch]$Update,
    [Parameter(Mandatory=$false)][switch]$Delete,
    [Parameter(Mandatory=$false)][switch]$Execute,
    [Parameter(Mandatory=$false)][switch]$All
  )
  Begin {
    If ( ($Grant -and $Revoke) -or (!$Grant -and !$Revoke) ) {
      Throw "Please specify one and only one of '-Grant' or '-Revoke'"
      Return
    }
    If ($Permission){
      If ($Permission -like "*CREATE*"){ $Create = $True }
      If ($Permission -like "*READ*"){ $Read = $True }
      If ($Permission -like "*UPDATE*"){ $Update = $True }
      If ($Permission -like "*DELETE*"){ $Delete = $True }
      If ($Permission -like "*EXECUTE*"){ $Execute = $True }
    }
    If ( (!$Create) -and (!$Read) -and (!$Update) -and (!$Delete) -and
         (!$Execute) -and (!$All) ) {
      Throw "Please specify at least one permission"
      Return
    }
    If ( (!$User) -and (!$Role) -and (!$UserLogin) -and (!$RoleId) ) {
      Throw  "Please specify at least one user or role"
      Return
    }
    If     ($Grant)  { $action = "grant_to" }
    Elseif ($Revoke) { $action = "revoke_from" }
    $operations = @()
    If ($Create  -or $All)  { $operations += 'CREATE'  }
    If ($Read    -or $All)  { $operations += 'READ'    }
    If ($Update  -or $All)  { $operations += 'UPDATE'  }
    If ($Delete  -or $All)  { $operations += 'DELETE'  }
    If ($Execute -or $All)  { $operations += 'EXECUTE' }
    $userLogins = @()
    If ($User) {
      $userLogins = $User | Select-Object -ExpandProperty 'login'
    }
    If ($UserLogin) {
      $userLogins += $UserLogin
    }
    $roleIds = @()
    If ($Role) {
      $RoleIds = $Role | Select-Object -ExpandProperty 'id'
    }
    If ($RoleId) {
      $roleIds += $RoleId
    }
  }
  Process {
    If ($Resource) {
     $Href = ($Resource | Select-Object -ExpandProperty '_href')
     $Href = ($Href | ForEach-Object {$_ -replace "^${StripUriPrefix}",'' })
    }
    foreach ($hrefItem in $Href) {
      foreach ($userLoginItem in $userLogins) {
        $uri = "/pulp/api/v2/permissions/actions/${action}_user/"
        $command = New-Object -Type PSCustomObject -Property @{'operations'=
          $operations;'login'=$userLoginItem;'resource'=$hrefItem}
        $null = Invoke-PulpRestMethod -Server $Server -Port $Port `
          -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
          -Uri $uri -Body (ConvertTo-Json -Depth 20 $command) -Method Post
      }
      foreach ($roleIdItem in $roleIds) {
        $uri = "/pulp/api/v2/permissions/actions/${action}_role/"
        $command = New-Object -Type PSCustomObject -Property @{'operations'=
          $operations;'role_id'=$roleIdItem;'resource'=$hrefItem}
        $null = Invoke-PulpRestMethod -Server $Server -Port $Port `
          -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
          -Uri $uri -Body (ConvertTo-Json -Depth 20 $command) -Method Post
      }
      Get-PulpPermission -Server $Server -Port $Port `
          -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
          -Href $hrefItem
    }
  }
}
