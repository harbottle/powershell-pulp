---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Invoke-PulpRestMethod.md
schema: 2.0.0
---

# Invoke-PulpRestMethod

## SYNOPSIS
Send an HTTP or HTTPS request to a Pulp RESTful web service.

## SYNTAX

```
Invoke-PulpRestMethod [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Uri] <String> [[-Method] <String>] [[-Body] <Object>] [[-ContentType] <String>] [[-InFile] <String>]
 [-TimeoutSec <Int32>] [<CommonParameters>]
```

## DESCRIPTION
The Invoke-RestMethod cmdlet sends HTTP and HTTPS requests to the Pulp
Representational State Transfer (REST) web service that returns richly
structured data.

## EXAMPLES

### Example 1
```
PS C:\> Invoke-PulpRestMethod -Server pulp.test -Port 443 -Protocol https -AuthenticationMethod certificate -Uri '/pulp/api/v2/users/' -Method Post -Body '{"login": "grace", "name": "Grace Hopper", "password":  "cobol"}'
```

Authenticate using certificate and POST
`'{"login": "grace", "name": "Grace Hopper", "password":  "cobol"}'` to
https://pulp.test/pulp/api/v2/users/ to create a new Pulp user.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Uri
Specifies the Uniform Resource Identifier (URI) of the Internet resource to
which the web request is sent.  URI should be relative to '/'.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: True
Position: 4
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Body
Specifies the body of the request. The body is the content of the request that
follows the headers. Pulp expects JSON format.

```yaml
Type: Object
Parameter Sets: (All)
Aliases:

Required: False
Position: 6
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ContentType
Specifies the content type of the web request.

If this parameter is omitted and the request method is POST,
Invoke-PulpRestMethod sets the content type to
"application/x-www-form-urlencoded". Otherwise, the content type is not
specified in the call.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 7
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -InFile
Gets the content of the web request from a file.

Enter a path and file name. If you omit the path, the default is the
current location.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 8
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Method
Specifies the method used for the web request. Valid values are Default, Delete,
Get, Head, Merge, Options, Patch, Post, Put, and Trace.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 5
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -TimeoutSec
Operation timeout in seconds. 

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.String
JSON-formatted body.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp objects.

## NOTES
NAME:  Invoke-PulpRestMethod

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
