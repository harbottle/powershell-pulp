---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpRpmSyncSchedule.md
schema: 2.0.0
---

# Get-PulpRpmSyncSchedule

## SYNOPSIS
Get Pulp RPM repo synchronization schedules.

## SYNTAX

### Objects
```
Get-PulpRpmSyncSchedule [-Server <String>] [-Port <Int32>] [-Protocol <String>]
 [-AuthenticationMethod <String>] -Repo <PSObject[]> [<CommonParameters>]
```

### Strings
```
Get-PulpRpmSyncSchedule [-Server <String>] [-Port <Int32>] [-Protocol <String>]
 [-AuthenticationMethod <String>] -RepoId <String[]> [<CommonParameters>]
```

## DESCRIPTION
This function returns synchronization schedules for the RPM repo IDs or RPM repo
objects provided. If no IDs or repo objects are provided, all RPM repo schedules
will be returned.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpRpmSyncSchedule
```

Get all Pulp RPM repo sync schedules.

### Example 2
```
PS C:\> Get-PulpRpmRepo 'epel*' | Get-PulpRpmSyncSchedule
```

Get all Pulp sync schedules for RPM repos with ID matching `epel*`.

### Example 3
```
PS C:\> Get-PulpRpmSyncSchedule -RepoID 'epel*'
```

Get all Pulp sync schedules for RPM repos with ID matching `epel*`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -RepoId
Pulp repo IDs.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: True
Position: Named
Default value: *
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Repo
Pulp repo objects.

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp synchronization schedule objects.

## NOTES
NAME:  Get-PulpRpmSyncSchedule

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
