---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpTask.md
schema: 2.0.0
---

# Get-PulpTask

## SYNOPSIS
This function returns tasks from a Pulp server.

## SYNTAX

### Strings (Default)
```
Get-PulpTask [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-TaskId] <String[]>] [-NoSpawnedTasks] [-All] [<CommonParameters>]
```

### Objects
```
Get-PulpTask [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-Task] <PSObject[]>] [-NoSpawnedTasks] [-All] [<CommonParameters>]
```

## DESCRIPTION
This function returns objects representing tasks from a Pulp server. 
If no task is specified, by default only running and waiting tasks will be returned. 
Use the -All switch to return all tasks.

By default recursion will be used to return "spawned tasks". 
Use the -NoSpawnedTasks switch to turn this off.

One of the following may be supplied:

- TaskId of the task or tasks to return.   - An object (or objects) containing a task_id property.   - An object (or objects) containing a spawned_tasks property.

## EXAMPLES

### Example 1
```
Get-PulpTask -TaskId 'a8434ca6-a5bc-495c-8dca-ab1c31e83dc7'
```

Get the task with ID 'a8434ca6-a5bc-495c-8dca-ab1c31e83dc7'

### Example 2
```
$tasks = Remove-PulpRpmRepo temp*; Start-Sleep -s 10; Get-PulpTask -Task $tasks
```

Remove some repos and then check they were successfully removed.

### Example 3
```
Get-PulpTask -All | Where-Object {[DateTime]$_.start_time -gt (Get-Date).AddHours(-1)}
```

Find all tasks started in the last hour.

## PARAMETERS

### -Server
The Pulp server to query.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Server).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
The port to query.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
The protocol to use.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Task
An object (or array of objects) containing task_id property.

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: False
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -TaskId
The ID of the task to return.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: False
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -NoSpawnedTasks
Disable return of spawned tasks.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -All
Switch to return all tasks.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### `System.Management.Automation.PSCustomObject[]`
Provide one or more objects containing a \`task_id\` property. 
All other object properties are ignored.

## OUTPUTS

### `System.Management.Automation.PSCustomObject[]`
Returns one or more objects representing Pulp tasks.

## NOTES
NAME:  Get-PulpTask

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
