#!/bin/bash
mkdir -p ~/.config
echo -e "[copr-cli]\nlogin = ${copr_login}\nusername = harbottle\ntoken = ${copr_token}\ncopr_url = https://copr.fedorainfracloud.org" > ~/.config/copr
copr-cli build main *.src.rpm
