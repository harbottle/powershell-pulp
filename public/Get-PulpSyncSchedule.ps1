# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpSyncSchedule {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Repo,

    [Parameter(Mandatory=$false, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$RepoId ='*',

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {}
  Process {
    If ($Repo) {
      $RepoId = $Repo | Select-Object -ExpandProperty id
    }
    If ($Type) {
      $repos = Get-PulpRepo -Server $server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Id $RepoId -Type $Type
    } Else {
      $repos = Get-PulpRepo -Server $server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Id $RepoId
    }
    Foreach ($repoItem in $repos){
      $repoId = $repoItem.id
      $importers = $repoItem | Select-Object -ExpandProperty importers
      Foreach ($importer in $importers){
        $importerId = $importer.id
        $uri = "/pulp/api/v2/repositories/${repoId}/importers/${importerId}/schedules/sync/"
        Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
          -AuthenticationMethod $AuthenticationMethod -Uri $uri
      }
    }
  }
}
