---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Remove-PulpIso.md
schema: 2.0.0
---

# Remove-PulpIso

## SYNOPSIS
Remove Pulp ISO content units.

## SYNTAX

```
Remove-PulpIso [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-ContentUnit] <PSObject[]> [-NoPublish] [<CommonParameters>]
```

## DESCRIPTION
This function removes Pulp ISO content units.  More precisely, it dissociates
content units from ISO repos. Content units to be removed must be supplied as
objects.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpIsoRepo internal | Get-PulpIso -Name 'windows*' | Remove-PulpIso
```

Remove all content units with name matching `windows*` from ISO repo `internal`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -ContentUnit
Pulp content unit objects.

```yaml
Type: PSObject[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 4
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -NoPublish
Do not publish the repositories after the operation.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp content unit objects.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp task objects.

## NOTES
NAME:  Remove-PulpIso

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
