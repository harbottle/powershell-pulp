# .ExternalHelp powershell-pulp-help.xml
Function Remove-PulpRole {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
    [PSCustomObject[]]$Role
  )
  Begin {}
  Process {
    $ids = $Role | Select-Object -ExpandProperty id
    Foreach ($id in $ids) {
      $null = Invoke-PulpRestMethod -Server $Server -Port $Port `
        -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
        -Uri "/pulp/api/v2/roles/${id}" -Method Delete
    }
  }
}
