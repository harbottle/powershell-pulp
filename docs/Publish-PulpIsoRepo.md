---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Publish-PulpIsoRepo.md
schema: 2.0.0
---

# Publish-PulpIsoRepo

## SYNOPSIS
Publish Pulp ISO repos.

## SYNTAX

### Objects
```
Publish-PulpIsoRepo [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Repo] <PSObject[]> [-Full] [<CommonParameters>]
```

### Strings
```
Publish-PulpIsoRepo [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Id] <String[]> [-Full] [<CommonParameters>]
```

## DESCRIPTION
This function publishes Pulp repositories, specified with either repo objects or
repo IDS. Resulting Pulp tasks are returned.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpIsoRepo | Publish-PulpIsoRepo
```

Publish all Pulp ISO repos.

### Example 2
```
PS C:\> Get-PulpIsoRepo 'archive*' | Publish-PulpIsoRepo
```

Publish all Pulp ISO repos matching ID `archive*`.

### Example 3
```
PS C:\> Publish-PulpIsoRepo -Id 'archive*'
```

Publish all Pulp ISO repos matching ID `archive*`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Id
Pulp repo IDs.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: True
Position: 0
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Repo
Pulp repo objects.

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: True
Position: 0
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Full
Publish from scratch, even if no changes were introduced since last publish.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp task objects.

## NOTES
NAME:  Publish-PulpIsoRepo

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
