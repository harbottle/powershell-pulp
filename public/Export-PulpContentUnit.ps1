# .ExternalHelp powershell-pulp-help.xml
Function Export-PulpContentUnit {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$false)]
    [string]$DownloadPath = (Get-Location).Path,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [PSCustomObject[]]$ContentUnit,

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {
    $null = (New-Item -ItemType Directory -Force -Path $DownloadPath)
    If ($Type -eq 'puppet'){ $Type = 'puppet_module' }
    $lastRepoId = ''
    $lastRelativeUrl = ''
  }
  Process {
    $unitId = $ContentUnit | Select-Object -ExpandProperty metadata |
                             Select-Object -ExpandProperty _id
    $unitType = $ContentUnit | Select-Object -ExpandProperty unit_type_id
    $repoId = $ContentUnit | Select-Object -ExpandProperty repo_id
    If (!$Type -or $Type -eq $unitType) {
      If ($unitType -eq 'rpm') {
        $downloadUrlBase =
          (Get-PulpLocalConfig -RpmDownloadUrlBase).rpmdownloadurlbase
        If ($repoId -eq $lastRepoId) {
          $relativeUrl = $lastRelativeUrl
        }
        Else {
          $relativeUrl = ((Get-PulpRpmRepo $repoId).distributors |
            Where-Object {
              $_.distributor_type_id -eq 'yum_distributor'
            })[0].config.relative_url
        }
        $lastRepoId = $repoId
        $lastRelativeUrl = $relativeUrl
        $filename = $ContentUnit.metadata.filename
        $url = "${downloadUrlBase}/${relativeUrl}/Packages/" +
          ($filename.ToLower())[0] + "/${filename}"
      }
      Elseif ($unitType -eq 'iso') {
        $downloadUrlBase =
          (Get-PulpLocalConfig -IsoDownloadUrlBase).isodownloadurlbase
        $filename = $ContentUnit.metadata.name
        $url = "${downloadUrlBase}/${repoId}/${filename}"
      }
      Elseif ($unitType -eq 'puppet_module') {
        $downloadUrlBase =
          (Get-PulpLocalConfig -PuppetDownloadUrlBase).puppetdownloadurlbase
        $author = $ContentUnit.metadata.author
        $name = $ContentUnit.metadata.name
        $version = $ContentUnit.metadata.version
        $filename = "${author}-${name}-${version}.tar.gz"
        $url = "${downloadUrlBase}/${repoId}/system/releases/" + $author[0] +
          "/${author}/${filename}"
      }
      Invoke-WebRequest -Uri $Url -OutFile "${DownloadPath}\${filename}"
    }
  }
}
