---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/New-PulpRpmRepo.md
schema: 2.0.0
---

# New-PulpRpmRepo

## SYNOPSIS
Create new Pulp RPM repos.

## SYNTAX

### Objects
```
New-PulpRpmRepo [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Repo] <Object[]> [-NoPublish] [<CommonParameters>]
```

### Strings
```
New-PulpRpmRepo [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Id] <String> [-DisplayName <String>] [-Description <String>] [-Note <String>] [-Feed <String>]
 [-Validate <Boolean>] [-FeedCaCert <String>] [-VerifyFeedSsl <Boolean>] [-FeedCert <String>]
 [-FeedKey <String>] [-ProxyHost <String>] [-ProxyPort <String>] [-ProxyUser <String>] [-ProxyPass <String>]
 [-MaxDownloads <Int32>] [-MaxSpeed <Int32>] [-ServeHttp <Boolean>] [-ServeHttps <Boolean>]
 [-RemoveMissing <Boolean>] [-AuthCa <String>] [-Skip <String>] [-BasicauthUser <String>]
 [-BasicauthPass <String>] [-RetainOldCount <Int32>] [-RelativeUrl <String>] [-ChecksumType <String>]
 [-GpgKey <String>] [-GenerateSqlite <Boolean>] [-HostCa <String>] [-AuthCert <String>] [-NoPublish]
 [<CommonParameters>]
```

## DESCRIPTION
This function creates new Pulp RPM repos.  Either of the following must be
supplied:

- Repo objects containing all the settings for the new RPM repos
- Repo IDs and settings supplied with relevant parameters

## EXAMPLES

### Example 1
```
PS C:\> New-PulpRpmRepo -Id epel7x86_64 -Feed 'https://dl.fedoraproject.org/pub/epel/7/x86_64/' -ServeHttp $true -RelativeUrl '/centos/7/x86_64/epel'
```

Create a new RPM repo `epel` configured to synchronize from upstream repo.

### Example 2
```
PS C:\> Get-PulpRpmRepo -Server oldserver.test | New-PulpRpmRepo
```

Use `New-PulpRpmRepo` to copy all RPM repos from `oldserver.test` to current
default Pulp server.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Repo
Pulp repo objects.

```yaml
Type: Object[]
Parameter Sets: Objects
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Id
Unique identifier; only alphanumeric, ., -, and _ allowed

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -DisplayName
User-readable display name (may contain i18n characters).

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Description
User-readable description (may contain i18n characters).

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Note
Notes to programmatically identify the resource; key-value pairs must be
separated by an equal sign (e.g. key=value).

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Feed
URL of the external source repository to sync.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Validate
If `$true`, the size and checksum of each synchronized file will be verified
against the repo metadata.

```yaml
Type: Boolean
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Skip
Comma-separated list of types to omit when synchronizing.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -FeedCaCert
CA certificate that should be used to verify the external repo server's SSL
certificate.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -VerifyFeedSsl
If `$true`, the feed's SSL certificate will be verified against the feed CA
cert.

```yaml
Type: Boolean
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -FeedCert
Certificate to use for authorization when accessing the external feed.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -FeedKey
Full path to the private key for feed_cert.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProxyHost
Proxy server url to use.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProxyPort
Port on the proxy server to make requests.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProxyUser
Username used to authenticate with the proxy server.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProxyPass
Password used to authenticate with the proxy server.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -BasicauthUser
Username used to authenticate with sync location via HTTP basic auth.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -BasicauthPass
Password used to authenticate with sync location via HTTP basic auth.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -MaxDownloads
Maximum number of downloads that will run concurrently.

```yaml
Type: Int32
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -MaxSpeed
Maximum bandwidth used per download thread, in bytes/sec, when synchronizing the
repo.

```yaml
Type: Int32
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -RemoveMissing
If "$true", units that were previously in the external feed but are no longer
found will be removed from the repository.

```yaml
Type: Boolean
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -RetainOldCount
Count indicating how many non-latest versions of a unit to keep in a repository.

```yaml
Type: Int32
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -RelativeUrl
Relative path the repository will be served from. Only alphanumeric characters,
forward slashes, underscores and dashes are allowed. It defaults to the relative
path of the feed URL or to the ID when there is no feed.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ServeHttp
If `$true`, the repository will be served over HTTP.

```yaml
Type: Boolean
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ServeHttps
If `$true`, the repository will be served over HTTPS.

```yaml
Type: Boolean
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ChecksumType
Type of checksum to use during metadata generation.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -GpgKey
GPG key used to sign and verify packages in the repository.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -GenerateSqlite
If `$true`, sqlite files will be generated for the repository metadata during
publish.

```yaml
Type: Boolean
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -HostCa
CA certificate that signed the repository hosts's SSL certificate when serving
over HTTPS.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthCa
CA certificate that should be used to verify client authentication certificates;
setting this turns on client authentication for the repository.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthCert
Entitlement certificate that will be given to bound consumers to grant access
to this repository.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -NoPublish
Do not publish the repositories after the operation.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

## NOTES
NAME:  New-PulpRpmRepo

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
