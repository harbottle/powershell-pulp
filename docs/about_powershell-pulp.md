﻿# powershell-pulp
## about_powershell-pulp

# SHORT DESCRIPTION
A Windows PowerShell module for Pulp.

# LONG DESCRIPTION
Manage Pulp from the comfort of uour PowerShell prompt.

For more information, visit: https://gitlab.com/harbottle/powershell-pulp
