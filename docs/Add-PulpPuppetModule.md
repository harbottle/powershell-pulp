---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Add-PulpPuppetModule.md
schema: 2.0.0
---

# Add-PulpPuppetModule

## SYNOPSIS
Upload Puppet modules to Pulp Puppet repositories.

## SYNTAX

### StringsPath (Default)
```
Add-PulpPuppetModule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepoId <String[]> [-Path] <String[]> [-NoPublish] [<CommonParameters>]
```

### ObjectsForge
```
Add-PulpPuppetModule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepo <PSObject[]> [-ForgeBaseUrl <String>] -Author <String> -Name <String> -Version <String>
 [-NoPublish] [<CommonParameters>]
```

### ObjectsUrl
```
Add-PulpPuppetModule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepo <PSObject[]> -Url <String> [-NoPublish] [<CommonParameters>]
```

### ObjectsPath
```
Add-PulpPuppetModule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepo <PSObject[]> [-Path] <String[]> [-NoPublish] [<CommonParameters>]
```

### StringsForge
```
Add-PulpPuppetModule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepoId <String[]> [-ForgeBaseUrl <String>] -Author <String> -Name <String> -Version <String>
 [-NoPublish] [<CommonParameters>]
```

### StringsUrl
```
Add-PulpPuppetModule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -TargetRepoId <String[]> -Url <String> [-NoPublish] [<CommonParameters>]
```

## DESCRIPTION
This function uploads one or more Puppet modules to one or more Puppet
repositories on a Pulp server.

One of the following must be supplied:

- A file path pattern for the modules to be uploaded.
- A URL for the module to be uploaded.
- The author, name and version of the module on the Puppet Forge (https://forge.puppet.com/).

One of the following must also be supplied as an argument or using the pipeline:

- Target repo IDs.
- Target repo objects

If multiple repos are supplied in any way (wildcards, arrays or pipeline),
modules will be uploaded to the first repo and then copied from that repo to the
other specified repos. This is to ensure the same module is not uploaded
multiple times.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpPuppetRepo modules* | Add-PulpPuppetModule C:\puppet\modules\*.tar.gz
```

Upload all modules in `C:\puppet\modules` to all Puppet repos matching ID `modules*`.

### Example 2
```
PS C:\> Add-PulpPuppetModule -Url 'https://forge.puppet.com/v3/files/puppetlabs-apache-1.11.0.tar.gz' -TargetRepoId localforge
```

Download https://forge.puppet.com/v3/files/puppetlabs-apache-1.11.0.tar.gz and
upload it to Puppet repo with ID `localforge`.

### Example 3
```
PS C:\> Add-PulpPuppetModule -Author puppetlabs -Name apache -Version 1.11.0 -TargetRepoId localforge
```

Download module puppetlabs-apache-1.11.0 from the Puppet Forge and upload it to
Puppet repo with ID `localforge`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -TargetRepo
Target repo objects.

```yaml
Type: PSObject[]
Parameter Sets: ObjectsForge, ObjectsUrl, ObjectsPath
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -TargetRepoId
Target repo IDs.

```yaml
Type: String[]
Parameter Sets: StringsPath, StringsForge, StringsUrl
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Path
Path of the modules to be added.

```yaml
Type: String[]
Parameter Sets: StringsPath, ObjectsPath
Aliases:

Required: True
Position: 0
Default value: None
Accept pipeline input: False
Accept wildcard characters: True
```

### -Url
URL of the module to be added.

```yaml
Type: String
Parameter Sets: ObjectsUrl, StringsUrl
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Author
Author of the module to be added from the Puppet Forge.

```yaml
Type: String
Parameter Sets: ObjectsForge, StringsForge
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Name
Name of the module to be added from the Puppet Forge.

```yaml
Type: String
Parameter Sets: ObjectsForge, StringsForge
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Version
Version of the module to be added from the Puppet Forge.

```yaml
Type: String
Parameter Sets: ObjectsForge, StringsForge
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ForgeBaseUrl
Puppet Forge base URL of the module to be added from the Puppet Forge.

```yaml
Type: String
Parameter Sets: ObjectsForge, StringsForge
Aliases:

Required: False
Position: Named
Default value: 'https://forge.puppet.com/v3/files/'
Accept pipeline input: False
Accept wildcard characters: False
```

### -NoPublish
Do not publish the target repositories after the operation.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp task objects.

## NOTES
NAME:  Add-PulpPuppetModule

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
