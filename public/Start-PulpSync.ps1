# .ExternalHelp powershell-pulp-help.xml
Function Start-PulpSync {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Repo,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$RepoId,

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {}
  Process {
    If ($Repo) {
      $RepoId = $Repo | Select-Object -ExpandProperty id
    }
    If ($Type) {
      $repos = Get-PulpRepo -Server $server -Port $Port `
                            -Protocol $Protocol `
                            -AuthenticationMethod $AuthenticationMethod `
                            -Id $RepoId -Type $Type
    } Else {
      $repos = Get-PulpRepo -Server $server -Port $Port `
                            -Protocol $Protocol `
                            -AuthenticationMethod $AuthenticationMethod `
                            -Id $RepoId
    }
    Foreach ($repo in $repos) {
      $syncId = $repo.id
      $uri = "/pulp/api/v2/repositories/${syncId}/actions/sync/"
      Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Uri $uri -Method Post
    }
  }
}
