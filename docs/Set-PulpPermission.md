---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Set-PulpPermission.md
schema: 2.0.0
---

# Set-PulpPermission

## SYNOPSIS
Set permissions assigned to Pulp resources.

## SYNTAX

### Strings (Default)
```
Set-PulpPermission [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-Href] <String[]>] [-User <PSObject[]>] [-Role <PSObject[]>] [-UserLogin <String[]>] [-RoleId <String[]>]
 [-Grant] [-Revoke] [-Permission <String>] [-Create] [-Read] [-Update] [-Delete] [-Execute] [-All]
 [<CommonParameters>]
```

### Objects
```
Set-PulpPermission [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Resource] <PSObject[]> [-StripUriPrefix <String>] [-User <PSObject[]>] [-Role <PSObject[]>]
 [-UserLogin <String[]>] [-RoleId <String[]>] [-Grant] [-Revoke] [-Permission <String>] [-Create] [-Read]
 [-Update] [-Delete] [-Execute] [-All] [<CommonParameters>]
```

## DESCRIPTION
This function grants or revokes permissions for one or more users or roles to
one or more Pulp resources.

At least one user or role must be supplied (as objects or strings)

One of the following must be supplied

- One or more Pulp resource hypertext references (href strings).
- One or more Pulp objects (e.g. a repo)

These represent the resources on which the permissions will be granted.

Grant or revoke setting is specified with one of the following switches:

- Grant
- Revoke

One of these switches must be specified.

Permissions are specified with the relevant switch/es:

- Create
- Read
- Update
- Delete
- Execute
- All

At least one permission switch must be specified

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpRpmRepo "centos6*" | Set-PulpPermission -User "jsmith" -Grant -Create -Read -Update -Delete -Execute
```

Grants all possible permissions to user with ID "jsmith" on all Pulp RPM  repositories with IDs starting with "centos6"

### Example 2
```
PS C:\> Set-PulpPermission -Resource (Get-PulpRpmRepo '*epel*') -User (Get-PulpUser 'jsmith') -Revoke -Delete
```

Revokes "delete" permissions from user with ID "jsmith" on all Pulp RPM repositories with IDs containing "epel".

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Href
Resource strings.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: False
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Resource
Resource object/s containing _href properties.

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -StripUriPrefix
Prefix string to remove from _href property of resource objects.

```yaml
Type: String
Parameter Sets: Objects
Aliases:

Required: False
Position: Named
Default value: /pulp/api
Accept pipeline input: False
Accept wildcard characters: False
```

### -UserLogin
User login string/s.

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -User
User object/s containing login property.

```yaml
Type: PSObject[]
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -RoleId
Role id string/s.

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Role
Role objects/s containing id property.

```yaml
Type: PSObject[]
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Grant
Grant permissions.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Revoke
Revoke permissions.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Permission
Comma-delimited permission string.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Create
Grant/revoke "create" permission.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Read
Grant/revoke "read" permission.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Update
Grant/revoke "update" permission.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Delete
Grant/revoke "delete" permission.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Execute
Grant/revoke "execute" permission.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -All
Grant/revoke all permissions.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp objects.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp permission objects.

### None

## NOTES
NAME:  Set-PulpPermission

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
