# .ExternalHelp powershell-pulp-help.xml
Function Set-PulpIsoRepo {
 [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [string]$Server,
    [int]$Port,
    [string]$Protocol,
    [string]$AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [object[]]$Repo,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$Id,

    [Parameter(Mandatory=$false)]
    [string]$DisplayName,

    [Parameter(Mandatory=$false)]
    [string]$Description,

    [Parameter(Mandatory=$false)]
    [string]$Note,

    [Parameter(Mandatory=$false)]
    [string]$Feed,

    [Parameter(Mandatory=$false)]
    [bool]$Validate,

    [Parameter(Mandatory=$false)]
    [string]$FeedCaCert,

    [Parameter(Mandatory=$false)]
    [bool]$VerifyFeedSsl,

    [Parameter(Mandatory=$false)]
    [string]$FeedCert,

    [Parameter(Mandatory=$false)]
    [string]$FeedKey,

    [Parameter(Mandatory=$false)]
    [string]$ProxyHost,

    [Parameter(Mandatory=$false)]
    [string]$ProxyPort,

    [Parameter(Mandatory=$false)]
    [string]$ProxyUser,

    [Parameter(Mandatory=$false)]
    [string]$ProxyPass,

    [Parameter(Mandatory=$false)]
    [int]$MaxDownloads,

    [Parameter(Mandatory=$false)]
    [int]$MaxSpeed,

    [Parameter(Mandatory=$false)]
    [bool]$ServeHttp,

    [Parameter(Mandatory=$false)]
    [bool]$ServeHttps,

    [Parameter(Mandatory=$false)]
    [bool]$RemoveMissing,

    [Parameter(Mandatory=$false)]
    [string]$AuthCa,

    [Parameter(Mandatory=$false)]
    [switch]$NoPublish
  )
  Begin {
    try {
      $outBuffer = $null
      if ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'Set-PulpRepo', [System.Management.Automation.CommandTypes]::Function)
      $scriptCmd = {& $wrappedCmd @PSBoundParameters -Type 'iso'}
      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    catch { throw }
  }
  Process {
    try {
      $steppablePipeline.Process($_)
    }
    catch {
      throw
    }
  }
  End {
    try {
      $steppablePipeline.End()
    }
    catch {
      throw
    }
  }
}
