#!/bin/bash
export version=$(grep 'ModuleVersion' powershell-pulp.psd1 | sed "s/ModuleVersion = '\(.*\)'.*/\1/g")
sed -i "s/__VERSION__/${version}/g" powershell-pulp.spec
mkdir -p rpmbuild/{SPECS,SOURCES}
cp *.spec rpmbuild/SPECS
tar --transform "s|^|/powershell-pulp-v${version}/|" --exclude-from=.pkgignore -czf rpmbuild/SOURCES/powershell-pulp-v${version}.tar.gz *
rpmbuild --define "_topdir $PWD/rpmbuild" --ba rpmbuild/SPECS/powershell-pulp.spec
if [[ -v GPG_PRIVATE_KEY ]]; then
. .ci/sign_rpm.sh rpmbuild/RPMS/*/*.rpm
. .ci/sign_rpm.sh rpmbuild/SRPMS/*.rpm
fi
mv rpmbuild/RPMS/*/*.rpm .
mv rpmbuild/SRPMS/*.rpm .
