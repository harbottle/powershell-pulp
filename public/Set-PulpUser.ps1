# .ExternalHelp powershell-pulp-help.xml
Function Set-PulpUser {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$User,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$Login,

    [Parameter(Mandatory=$false, ParameterSetName="Objects")]
    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Name,

    [Parameter(Mandatory=$false, ParameterSetName="Objects")]
    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Password
  )
  Begin {}
  Process {
    If ($User) { $Login =  ($User | Select-Object -ExpandProperty login) }
    Foreach ($l in $Login) {
      $delta = New-Object -TypeName PSCustomObject
      If ($Name){
        Add-Member -InputObject $delta -MemberType NoteProperty -Name name -Value $Name
      }
      If ($Password) {
        Add-Member -InputObject $delta -MemberType NoteProperty -Name password -Value $Password
      }
      $uri = "/pulp/api/v2/users/${l}/"
      $userDelta = New-Object -TypeName PSCustomObject -Property @{delta=$delta}
      Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Uri $uri -Method Put `
        -Body (ConvertTo-Json $userDelta)
    }
  }
}
