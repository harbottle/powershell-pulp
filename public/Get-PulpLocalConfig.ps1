# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpLocalConfig {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)] [string]$File = '~\.pulp\admin.json',
    [Parameter(Mandatory=$false)] [switch]$Server,
    [Parameter(Mandatory=$false)] [switch]$Port,
    [Parameter(Mandatory=$false)] [switch]$Protocol,
    [Parameter(Mandatory=$false)] [switch]$RpmDownloadUrlBase,
    [Parameter(Mandatory=$false)] [switch]$IsoDownloadUrlBase,
    [Parameter(Mandatory=$false)] [switch]$PuppetDownloadUrlBase,
    [Parameter(Mandatory=$false)] [switch]$Username,
    [Parameter(Mandatory=$false)] [switch]$Password,
    [Parameter(Mandatory=$false)] [switch]$AuthenticationMethod,
    [Parameter(Mandatory=$false)] [switch]$SkipCertificateCheck
  )
  $defaults = @{'port'='443';'protocol'='https';'username'=$env:USERNAME;'authenticationmethod'='certificate';'skipcertificatecheck'=$False}
  $settings = New-Object -TypeName PSCustomObject
  $readSettings = New-Object -TypeName PSCustomObject
  $params = $PSBoundParameters.GetEnumerator()|
            Where-Object {($_.Key -ne 'File')}
  if (Test-Path $File){
    try {
      $readSettings = Get-Content -Raw '~\.pulp\admin.json' | ConvertFrom-Json
    } catch {}
  }
  foreach ($parameter in $params){
    $setting = $parameter.Key.ToLower()
    $value = $parameter.Value
    if ($value){
      if ($readSettings.$setting -ne $null) {
        Add-Member -InputObject $settings -MemberType NoteProperty `
                   -Name $setting -Value $readSettings.$setting
      }
      elseif ($defaults.$setting -ne $null) {
        Add-Member -InputObject $settings -MemberType NoteProperty `
                   -Name $setting -Value $defaults.$setting}
      else {
        If ($setting -eq 'Password') {
          $credential = Get-Credential -user ((Get-PulpLocalConfig -Username).Username) -Message "Enter Pulp password (username not used here)"
          Add-Member -InputObject $settings -MemberType NoteProperty `
                     -Name $setting -Value ($credential.Password | ConvertFrom-SecureString)
        }
        Else {
          Add-Member -InputObject $settings -MemberType NoteProperty `
                     -Name $setting -Value (Read-Host "$setting")
        }
      }
    }
  }
  if ($params.count -eq 0){
    foreach ($default in $defaults.GetEnumerator()){
      $setting = $default.Name
      $value = $default.Value
      Add-Member -InputObject $settings -MemberType NoteProperty `
                 -Name $setting -Value $value -Force
    }
    $readSettings.PSObject.Properties |
    foreach-object {
      $setting = $_.Name
      $value = $_.Value
      Add-Member -InputObject $settings -MemberType NoteProperty `
                 -Name $setting -Value $value -Force
    }
  }
  Return $settings
}
