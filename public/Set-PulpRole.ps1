# .ExternalHelp powershell-pulp-help.xml
Function Set-PulpRole {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Role,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$Id,

    [Parameter(Mandatory=$false, ParameterSetName="Objects")]
    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$DisplayName,

    [Parameter(Mandatory=$false, ParameterSetName="Objects")]
    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Description
  )
  Begin {}
  Process {
    If ($Role) { $Id =  ($Role | Select-Object -ExpandProperty id) }
    Foreach ($i in $Id) {
      $delta = New-Object -TypeName PSCustomObject
      If ($DisplayName){
        Add-Member -InputObject $delta -MemberType NoteProperty -Name display_name -Value $DisplayName
      }
      If ($Description) {
        Add-Member -InputObject $delta -MemberType NoteProperty -Name description -Value $Description
      }
      $uri = "/pulp/api/v2/roles/${i}/"
      $roleDelta = New-Object -TypeName PSCustomObject -Property @{delta=$delta}
      Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Uri $uri -Method Put `
        -Body (ConvertTo-Json $roleDelta)
    }
  }
}
