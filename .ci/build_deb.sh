#!/bin/bash
export version=$(grep 'ModuleVersion' powershell-pulp.psd1 | sed "s/ModuleVersion = '\(.*\)'.*/\1/g")
sed -i "s/__VERSION__/${version}/g" control
export iteration=$(grep 'Version' control | sed 's/Version: .*-\(.*\)/\1/g')
mkdir -p packageroot/DEBIAN
mkdir -p packageroot/usr/local/share/powershell/Modules/powershell-pulp/${version}
cp control packageroot/DEBIAN
rsync -avq --exclude-from=.pkgignore . packageroot/usr/local/share/powershell/Modules/powershell-pulp/${version}
dpkg-deb -b packageroot powershell-pulp_${version}-${iteration}_all.deb
