---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpCertificate.md
schema: 2.0.0
---

# Get-PulpCertificate

## SYNOPSIS
Get user certificate.

## SYNTAX

```
Get-PulpCertificate [-Server <String>] [-Port <Int32>] [-Protocol <String>] [[-Username] <String>] [-Force]
 [<CommonParameters>]
```

## DESCRIPTION
This function gets a valid client certificate for the specified server from the
user's certificate store. If a valid vertificate is not available the function
requests a new certificate from the Pulp server and saves it. The user will be
prompted for a username and password.

## EXAMPLES

### Example 1
```
Get-PulpCertificate -Server pulpserver.domain
```

Prompts for username and password and gets a certificate from https://pulpserver.domain

### Example 2
```
Get-PulpCertificate -Server pulpserver.domain -Port 99999
```

Prompts for username and password and gets a certificate from https://pulpserver.domain:99999

### Example 3
```
Get-PulpCertificate -Server pulpserver.domain -Protocol http
```

Prompts for username and password and gets a certificate from http://pulpserver.domain

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -Username
Pulp login.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 4
Default value: (Get-PulpLocalConfig -Username).Username
Accept pipeline input: False
Accept wildcard characters: False
```

### -Force
Request a new certificate even if a valid one is available in the user
certificate store.
 
```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### None

## OUTPUTS

### System.Security.Cryptography.X509Certificates.X509Certificate2
Pulp user certificate object.

## NOTES
NAME:  Get-PulpCertificate

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
