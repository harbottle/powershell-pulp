# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpBasicAuthHeaders {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string] $Protocol = (Get-PulpLocalConfig -Protocol).Protocol
  )
  $config = Get-PulpLocalConfig
  if ($config.password -and
      $Server -eq (Get-PulpLocalConfig -Server).Server -and
      $Port -eq (Get-PulpLocalConfig -Port).Port -and
      $Protocol -eq (Get-PulpLocalConfig -Protocol).Protocol) {
    $username = (Get-PulpLocalConfig -Username).username
    $securePassword = $config.password | ConvertTo-SecureString
    $password = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword))
  } Else {
     $credential = Get-Credential -Username `
       (Get-PulpLocalConfig -Username).username -Message `
       "Enter Pulp username and password"
     $password = $credential.GetNetworkCredential().password
     $username = $credential.UserName
  }
  $base64Cred = [System.Convert]::ToBase64String(
     [Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $username,$password)))
  Return @{Authorization=("Basic {0}" -f $base64Cred)}
}
