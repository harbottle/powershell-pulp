---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpPermission.md
schema: 2.0.0
---

# Get-PulpPermission

## SYNOPSIS
Get current permissions assigned to Pulp resources.

## SYNTAX

### Strings (Default)
```
Get-PulpPermission [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-Href] <String[]>] [<CommonParameters>]
```

### Objects
```
Get-PulpPermission [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Resource] <PSObject[]> [-StripUriPrefix <String>] [<CommonParameters>]
```

## DESCRIPTION
This function returns objects representing permissions currently assigned to one
or more pulp resources.

One of the following must be supplied

- One or more Pulp resource hypertext references (href strings).
- One or more Pulp objects (e.g. a repo)

These represent the resources to query for current assigned permisssions.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpRpmRepo "centos6*" | Get-PulpPermission
```

Return current permissions assigned to all Pulp RPM repositories with IDs
starting with "centos6".

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Href
Pulp resource hypertext references (href strings).

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: False
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Resource
Pulp objects (e.g. repos).

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -StripUriPrefix
Prefix string to remove from _href property of Pulp objects.

```yaml
Type: String
Parameter Sets: Objects
Aliases:

Required: False
Position: Named
Default value: /pulp/api
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp objects.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp permission objects.

## NOTES
NAME:  Get-PulpPermission

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
