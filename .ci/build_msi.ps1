(Get-Content ./.wix.json) -replace '__VERSION__', ((Import-PowerShellDataFile ./powershell-pulp.psd1).ModuleVersion) | Set-Content ./.wix.json
Start-WixBuild -Increment 0 -Exclude (Get-Content .pkgignore)
