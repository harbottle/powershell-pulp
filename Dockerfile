FROM mcr.microsoft.com/powershell:latest
LABEL maintainer="harbottle <harbottle@room3d3.com>"
ARG version=''

RUN mkdir -p /usr/local/share/powershell/Modules/powershell-pulp/${version}
RUN mkdir -p /tmp/module
COPY ./ /tmp/module/
RUN pwsh -c '& {Copy-Item \tmp\module\* \usr\local\share\powershell\Modules\powershell-pulp\$env:version -Recurse -Exclude (Get-Content \tmp\module\.pkgignore)}'
RUN pwsh -c '& {Import-Module powershell-pulp}'
