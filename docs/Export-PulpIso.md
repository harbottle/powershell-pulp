---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Export-PulpIso.md
schema: 2.0.0
---

# Export-PulpIso

## SYNOPSIS
Download ISO files from Pulp Repositories.

## SYNTAX

```
Export-PulpIso [[-Server] <String>] [[-Port] <Int32>] [[-Protocol] <String>] [[-AuthenticationMethod] <String>]
 [[-DownloadPath] <String>] [-ContentUnit] <PSObject[]> [<CommonParameters>]
```

## DESCRIPTION
This function downloads one or more ISO files from Pulp repositories.

## EXAMPLES

### Example 1
```powershell
PS C:\> Get-PulpIsoRepo msi | Get-PulpIso -Name '*foo*' | Export-PulpIso -DownloadPath ~/Downloads/export2
```

Download all ISO files matching `*foo*` from `msi` repo to directory `~/Downloads/export2`.

## PARAMETERS

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 3
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -ContentUnit
Content unit objects to be downloaded.

```yaml
Type: PSObject[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 5
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -DownloadPath
Folder to which content units should be downloaded.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 4
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: 1
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 2
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 0
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSObject[]
Pulp content unit objects.

## OUTPUTS

### None

## NOTES
NAME:  Export-PulpIso

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
