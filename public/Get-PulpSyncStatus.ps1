# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpSyncStatus {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Repo,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$RepoId = '*',

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {}
  Process {
    If ($Repo) {
      $RepoId = $Repo | Select-Object -ExpandProperty id
    }
    If ($Type) {
      $repos = Get-PulpRepo -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Id $RepoId -Type $Type
    } Else {
      $repos = Get-PulpRepo -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Id $RepoId
    }
    foreach ($r in $repos) {
      $syncId = $r.id
      $uri = "/pulp/api/v2/tasks/search/"
      $search = New-Object -Type PSCustomObject -Property @{'criteria'=
        New-Object -Type PSCustomObject -Property @{'filters'=
        New-Object -Type PSCustomObject -Property @{'state'=
        New-Object -Type PSCustomObject -Property @{'$in'=
        @('finished', 'error', 'canceled', 'skipped')};'tags'=
        New-Object -Type PSCustomObject -Property @{'$all'=
        @("pulp:repository:${syncId}",'pulp:action:sync')}}}}
      $result = Invoke-PulpRestMethod -Server $Server -Port $Port `
        -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
        -Uri $uri -Method Post -Body (ConvertTo-Json -Depth 20 $search)
      Get-PulpTask -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod $result
    }
  }
}
