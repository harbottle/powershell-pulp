# Run these commands to update help files:

## Generate help files for new commands without overwiriting existing help files
New-MarkdownHelp -Module powershell-pulp -OutputFolder .\docs
## Update existing commands
Update-MarkdownHelp .\docs

## (Make any manual edits to markdown help files now)

## Update xml help file
New-ExternalHelp .\docs -OutputPath en-US\ -Force
