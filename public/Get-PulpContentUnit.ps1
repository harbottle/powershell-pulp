# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpContentUnit {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string] $Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$false)]
    [string]$Name = '*',

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Repo,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$RepoId,

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {
    If ($Type -eq 'puppet') {
      $Type = 'puppet_module'
    }
    # For puppet modules, break name into author, name, version
    $modAuthor = $Name.split("-")[0]
    $modName = if ($Name.split("-")[1] -ne $null) {$Name.split("-")[1]} else {'*'}
    $modVersion = if ($Name.split("-")[2] -ne $null) {$Name.split("-")[2]} else {'*'}
  }
  Process {
    If ($Repo) { # Object-based parameter
      $RepoId = $Repo | Select-Object -ExpandProperty id
    }
    $repos = Get-PulpRepo -Server $server -Port $Port -Protocol $Protocol `
             -AuthenticationMethod $AuthenticationMethod -Id $RepoId
    Foreach ($repo in $repos) {
      $repoId = $repo.id
      $uri = "/pulp/api/v2/repositories/${repoId}/search/units/"
      $body = '{"criteria": {"filters": {"unit": {}}}}'
      $units = Invoke-PulpRestMethod -Server $Server -Port $Port `
               -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
               -Uri $uri -Method Post -Body $body
      # Filter returned units by type requested
      If ($Type) {$units = $units | Where-Object {($_.unit_type_id -eq $Type)}}
      # Filter returned units and return
      $units | Where-Object {
        (  # rpm units are filtered by filename
          ($_.unit_type_id -eq 'rpm') -and
          ($_.metadata.filename -like $Name)
        ) -or
        ( # puppet units are filtered by author-name-version
          ($_.unit_type_id -eq 'puppet_module') -and
          ($_.metadata.author -like $modAuthor) -and
          ($_.metadata.name -like $modName) -and
          ($_.metadata.version -like $modVersion)
        ) -or
        ( # anything else is filtered by name
          ($_.unit_type_id -ne 'rpm') -and
          ($_.unit_type_id -ne 'puppet_module') -and
          ($_.metadata.name -like $Name)
        )
      }
    }
  }
}
