# .ExternalHelp powershell-pulp-help.xml
Function Stop-PulpTask {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true)]
    [PSCustomObject[]]$Task
  )
  Begin {}
  Process {
    $taskId = $Task | Select-Object -ExpandProperty task_id
    foreach ($i in $taskId) {
      $uri = "/pulp/api/v2/tasks/${i}/"
      Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Uri $uri -Method Delete
    }
  }
}
