---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpLocalConfig.md
schema: 2.0.0
---

# Get-PulpLocalConfig

## SYNOPSIS
Get the local powershell-pulp configuration.

## SYNTAX

```
Get-PulpLocalConfig [[-File] <String>] [-Server] [-Port] [-Protocol] [-RpmDownloadUrlBase]
 [-IsoDownloadUrlBase] [-PuppetDownloadUrlBase] [-Username] [-Password] [-AuthenticationMethod]
 [-SkipCertificateCheck] [<CommonParameters>]
```

## DESCRIPTION
This function returns an object representing the local powershell-pulp
configuration. Configuration is returned from (in order of preference):

- a JSON formatted config file (default location: '~\.pulp\admin.json')
- sensible default values
- user input

Individual configuration values can also be selected.  If no configuration
values are selected, all values contained in the file and defaults will be
returned, but the user will not be prompted for missing values.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpLocalConfig
```

Gets local powershell-pulp configuration from '~\.pulp\admin.json'.

### Example 2
```
PS C:\> (Get-PulpLocalConfig -Server).server
```

Gets the Pulp server hostname from '~\.pulp\admin.json'.

## PARAMETERS

### -File
An alternative configuration file.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 1
Default value: ~\.pulp\admin.json
Accept pipeline input: False
Accept wildcard characters: False
```

### -Server
Include Pulp server hostname in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Include Pulp server port in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Include Pulp server protocol in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Username
Include Pulp server username value in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Password
Include Pulp server encrypted password in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Include Pulp server authentication method in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -SkipCertificateCheck
Include SkipCertificateCheck boolean in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -IsoDownloadUrlBase
Include Pulp server IsoDownloadUrlBase in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -PuppetDownloadUrlBase
Include Pulp server PuppetDownloadUrlBase in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -RpmDownloadUrlBase
Include Pulp server RpmDownloadUrlBase in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### None

## OUTPUTS

### System.Management.Automation.PSCustomObject
powershell-pulp configuration object.

## NOTES
NAME: Get-PulpLocalConfig

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
