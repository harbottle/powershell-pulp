# .ExternalHelp powershell-pulp-help.xml
Function Publish-PulpIsoRepo {
  [CmdletBinding()]
  Param(
    [string]$Server,
    [int]$Port,
    [string]$Protocol,
    [string]$AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Repo,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$Id,

    [Parameter(Mandatory=$false)]
    [switch]$Full
  )
  Begin {
    try {
      $outBuffer = $null
      if ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'Publish-PulpRepo', [System.Management.Automation.CommandTypes]::Function)
      $scriptCmd = {& $wrappedCmd @PSBoundParameters -Type 'iso'}
      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    catch { throw }
  }
  Process {
    try {
      $steppablePipeline.Process($_)
    }
    catch {
      throw
    }
  }
  End {
    try {
      $steppablePipeline.End()
    }
    catch {
      throw
    }
  }
}
