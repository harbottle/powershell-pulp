# .ExternalHelp powershell-pulp-help.xml
Function New-PulpRole {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,
    
    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
    [string[]]$Id,
    
    [Parameter(Mandatory=$false)]
    [string]$DisplayName,
    
    [Parameter(Mandatory=$false)]
    [string]$Description
  )
  Begin { $uri = "/pulp/api/v2/roles/" }
  Process {
    Foreach ($i in $Id) {
      $role = New-Object -TypeName PSCustomObject -Property @{
        'role_id'=$i;
        'display_name'=$i;
        'description'=$i
      }
      if ($DisplayName){ $role.display_name = $DisplayName}
      if ($Description) { $role.description = $Description }
      Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Uri $uri -Method Post `
        -Body (ConvertTo-Json $role)
    }
  }
}
