---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Remove-PulpOrphan.md
schema: 2.0.0
---

# Remove-PulpOrphan

## SYNOPSIS
Remove Pulp orphaned units.

## SYNTAX

### Objects
```
Remove-PulpOrphan [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -Orphan <PSObject[]> [-Type <String>] [<CommonParameters>]
```

### Strings
```
Remove-PulpOrphan [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-All] [-Type <String>] [<CommonParameters>]
```

## DESCRIPTION
This function removes Pulp orphaned units.  All orphaned units (optionally of a
specified type) can be removed or the orphaned unit objects can be supplied as
an input to the function.

## EXAMPLES

### Example 1
```powershell
PS C:\> Get-PulpOrphan -Type srpm -Name Foo | Remove-PulpOrphan
```

Remove all orphaned units with specified type and name.

### Example 2
```powershell
PS C:\> Remove-PulpOrphan -Type iso -All
```

Remove all orphaned units with specified type.

## PARAMETERS

### -All
Remove all orphaned units (instead of supplying objects as an input).

```yaml
Type: SwitchParameter
Parameter Sets: Strings
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Orphan
Orphaned unit objects to remove.

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Type
Type of orphaned units to remove.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSObject[]
Pulp orphaned units.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp task objects.

## NOTES
NAME:  Remove-PulpOrphan

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
