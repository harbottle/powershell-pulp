# .ExternalHelp powershell-pulp-help.xml
Function Set-PulpRepo {
 [Cmdletbinding(DefaultParameterSetName='Strings')]
 Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [object[]]$Repo,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$Id,

    [Parameter(Mandatory=$false)]
    [string]$DisplayName,
    # API: delta.display_name

    [Parameter(Mandatory=$false)]
    [string]$Description,
    # API: delta.description

    [Parameter(Mandatory=$false)]
    [string]$Note,
    # API: notes{}

    [Parameter(Mandatory=$false)]
    [string]$Feed,
    # API: importer_config.feed

    [Parameter(Mandatory=$false)]
    [bool]$Validate,
    # API: importer_config.validate

    [Parameter(Mandatory=$false)]
    [string]$FeedCaCert,
    # API: importer_config.ssl_ca_cert

    [Parameter(Mandatory=$false)]
    [bool]$VerifyFeedSsl,
    # API: importer_config.ssl_validation

    [Parameter(Mandatory=$false)]
    [string]$FeedCert,
    # API: importer_config.ssl_client_cert

    [Parameter(Mandatory=$false)]
    [string]$FeedKey,
    # API: importer_config.ssl_client_key

    [Parameter(Mandatory=$false)]
    [string]$ProxyHost,
    # API: importer_config.proxy_host

    [Parameter(Mandatory=$false)]
    [string]$ProxyPort,
    # API: importer_config.proxy_port

    [Parameter(Mandatory=$false)]
    [string]$ProxyUser,
    # API: importer_config.proxy_username

    [Parameter(Mandatory=$false)]
    [string]$ProxyPass,
    # API: importer_config.proxy_password

    [Parameter(Mandatory=$false)]
    [int]$MaxDownloads,
    # API: importer_config.max_downloads

    [Parameter(Mandatory=$false)]
    [int]$MaxSpeed,
    # API: importer_config.max_speed

    [Parameter(Mandatory=$false)]
    [bool]$ServeHttp,
    # API (rpm):    distributor_configs.yum_distributor.http
    #               distributor_configs.export_distributor.http
    # API (puppet): distributor_configs.puppet_distributor.http
    # API (iso):    distributor_configs.iso_distributor.http

    [Parameter(Mandatory=$false)]
    [bool]$ServeHttps,
    # API (rpm):    distributor_configs.yum_distributor.https
    #               distributor_configs.export_distributor.https
    # API (puppet): distributor_configs.puppet_distributor.https
    # API (iso):    distributor_configs.iso_distributor.https

    [Parameter(Mandatory=$false)]
    [bool]$RemoveMissing,
    # Only: rpm, iso
    # API: importer_config.remove_missing

    [Parameter(Mandatory=$false)]
    [string]$AuthCa,
    # Only: rpm, iso
    # API (rpm):    distributor_configs.yum_distributor.auth_ca
    #               distributor_configs.export_distributor.auth_ca
    # API (puppet): distributor_configs.puppet_distributor.auth_ca
    # API (iso):    distributor_configs.iso_distributor.auth_ca

    [Parameter(Mandatory=$false)]
    [string]$Skip,
    # Only: rpm
    # API: importer_config.type_skip_list[]
    #      distributor_configs.yum_distributor.skip[]
    #      distributor_configs.export_distributor.skip[]
    # Valid: 'rpm', 'drpm', 'distribution', 'erratum'

    [Parameter(Mandatory=$false)]
    [string]$BasicauthUser,
    # Only: rpm
    # API: importer_config.basic_auth_username

    [Parameter(Mandatory=$false)]
    [string]$BasicauthPass,
    # Only: rpm
    # API: importer_config.basic_auth_password

    [Parameter(Mandatory=$false)]
    [int]$RetainOldCount,
    # Only: rpm
    # API: importer_config.retain_old_count

    [Parameter(Mandatory=$false)]
    [string]$RelativeUrl,
    # Only: rpm
    # API (rpm): distributor_configs.yum_distributor.relative_url
    #            distributor_configs.export_distributor.relative_url

    [Parameter(Mandatory=$false)]
    [string]$ChecksumType,
    # Only: rpm
    # API (rpm): distributor_configs.yum_distributor.checksum_type
    #            distributor_configs.export_distributor.checksum_type

    [Parameter(Mandatory=$false)]
    [string]$GpgKey,
    # Only: rpm
    # API (rpm): distributor_configs.yum_distributor.gpgkey
    #            distributor_configs.export_distributor.gpgkey

    [Parameter(Mandatory=$false)]
    [bool]$GenerateSqlite,
    # Only: rpm
    # API (rpm): distributor_configs.yum_distributor.generate_sqlite
    #            distributor_configs.export_distributor.generate_sqlite

    [Parameter(Mandatory=$false)]
    [string]$HostCa,
    # Only: rpm
    # API (rpm): distributor_configs.yum_distributor.https_ca
    #            distributor_configs.export_distributor.https_ca

    [Parameter(Mandatory=$false)]
    [string]$AuthCert,
    # Only: rpm
    # API (rpm): distributor_configs.yum_distributor.auth_cert
    #            distributor_configs.export_distributor.auth_cert

    [Parameter(Mandatory=$false)]
    [switch]$NoPublish,

    [Parameter(Mandatory=$true)]
    [string]$Type
  )
  Begin {}
  Process {
    $defaultUpdate = @{}
    $defaultUpdate.'rpm' = ConvertFrom-Json @'
      {
        "delta":{},
        "importer_config":{},
        "distributor_configs":{
          "export_distributor":{},
          "yum_distributor":{}
        }
      }
'@
    $defaultUpdate.'iso' = ConvertFrom-Json @'
      {
        "delta":{},
        "importer_config":{},
        "distributor_configs":{
          "iso_distributor":{}
        }
      }
'@
    $defaultUpdate.'puppet' = ConvertFrom-Json @'
      {
        "delta":{
          "bg": false
        },
        "importer_config":{},
        "distributor_configs":{
          "puppet_distributor":{}
        }
      }
'@
    If ($Repo) {
      $Id = $Repo | Select-Object -ExpandProperty id
    }
    $repos = Get-PulpRepo -Server $server -Port $Port -Protocol $Protocol `
             -AuthenticationMethod $AuthenticationMethod -Id $Id -Type $Type
    Foreach ($repo in $repos) {
      $repoId = $repo.id
      $update = $defaultUpdate."${Type}"

      # REST attributes for all repo types
      If ($DisplayName) {
        Add-Member -InputObject $update.delta -MemberType NoteProperty -Name 'display_name' -Value $DisplayName
      }
      If ($Description) {
        Add-Member -InputObject $update.delta -MemberType NoteProperty -Name 'description' -Value $Description
      }
      If ($Note){
        Add-Member -InputObject $update.delta -MemberType NoteProperty -Name 'notes' -Value $null
        Foreach ($noteItem in ($Note -replace '\s').split(",")){
          $noteName =  $noteItem.split("=")[0]
          $noteValue = $noteItem.split("=")[1]
          Add-Member -InputObject $update.delta.notes -MemberType NoteProperty -Name $noteName -Value $noteValue
        }
      }
      If ($Feed) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'feed' -Value $feed
      }
      If ($FeedCaCert) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'ssl_ca_cert' -Value $FeedCaCert
      }
      If ($FeedCert) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'ssl_client_cert' -Value $FeedCert
      }
      If ($FeedKey) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'ssl_client_key' -Value $FeedKey
      }
      If ($ProxyHost) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'proxy_host' -Value $ProxyHost
      }
      If ($ProxyPort) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'proxy_port' -Value $ProxyPort
      }
      If ($ProxyUser) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'proxy_username' -Value $ProxyUser
      }
      If ($ProxyPass) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'proxy_password' -Value $ProxyPass
      }
      If ($MaxDownloads) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'max_downloads' -Value $MaxDownloads
      }
      If ($MaxSpeed) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'max_speed' -Value $MaxSpeed
      }
      If ($PSBoundParameters.ContainsKey('Validate')) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'validate' -Value $Validate
      }
      If ($PSBoundParameters.ContainsKey('VerifyFeedSsl')) {
        Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'ssl_validation' -Value $VerifyFeedSsl
      }

      # REST attributes for iso and puppet repos only
      If ($Type -eq 'puppet' -or $Type -eq 'iso') {
        If ($PSBoundParameters.ContainsKey('ServeHttp')) {
          Add-Member -Force -InputObject $update.distributor_configs."${Type}_distributor" -MemberType NoteProperty -Name 'serve_http' -Value $ServeHttp
        }
        If ($PSBoundParameters.ContainsKey('ServeHttps')) {
          Add-Member -Force -InputObject $update.distributor_configs."${Type}_distributor" -MemberType NoteProperty -Name 'serve_https' -Value $ServeHttps
        }
      }

      # REST attributes for iso and rpm repos only
      If ($Type -eq 'rpm' -or $Type -eq 'iso') {
        If ($PSBoundParameters.ContainsKey('RemoveMissing')) {
          Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'remove_missing' -Value $RemoveMissing
        }
      }

      # REST attributes for iso repos only
      If ($Type -eq 'iso') {
        If ($auth_ca){
          Add-Member -InputObject $update.distributor_configs."${Type}_distributor" -MemberType NoteProperty -Name 'auth_ca' -Value $AuthCa
        }
      }

      # REST attributes for rpm repos only
      If ($Type -eq 'rpm') {
        If ($BasicauthUser) {
          Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'basic_auth_username' -Value $BasicauthUser
        }
        If ($BasicauthPass) {
          Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'basic_auth_password' -Value $BasicauthPass
        }
        If ($RetainOldCount) {
          Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'retain_old_count' -Value $RetainOldCount
        }
        If ($Skip) {
          Add-Member -InputObject $update.importer_config -MemberType NoteProperty -Name 'skip' -Value ($Skip -replace '\s').split(',')
        }
        If ($RelativeUrl){
          Add-Member -InputObject $update.distributor_configs.yum_distributor -MemberType NoteProperty -Name 'relative_url' -Value $RelativeUrl
          Add-Member -InputObject $update.distributor_configs.export_distributor -MemberType NoteProperty -Name 'relative_url' -Value $RelativeUrl
        }
        If ($ChecksumType){
          Add-Member -InputObject $update.distributor_configs.yum_distributor -MemberType NoteProperty -Name 'checksum_type' -Value $ChecksumType
          Add-Member -InputObject $update.distributor_configs.export_distributor -MemberType NoteProperty -Name 'checksum_type' -Value $ChecksumType
        }
        If ($GpgKey){
          Add-Member -InputObject $update.distributor_configs.yum_distributor -MemberType NoteProperty -Name 'gpgkey' -Value $GpgKey
          Add-Member -InputObject $update.distributor_configs.export_distributor -MemberType NoteProperty -Name 'gpgkey' -Value $GpgKey
        }
        If ($HostCa){
          Add-Member -InputObject $update.distributor_configs.yum_distributor -MemberType NoteProperty -Name 'https_ca' -Value $HostCa
          Add-Member -InputObject $update.distributor_configs.export_distributor -MemberType NoteProperty -Name 'https_ca' -Value $HostCa
        }
        If ($auth_ca){
          Add-Member -InputObject $update.distributor_configs.yum_distributor -MemberType NoteProperty -Name 'auth_ca' -Value $AuthCa
          Add-Member -InputObject $update.distributor_configs.export_distributor -MemberType NoteProperty -Name 'auth_ca' -Value $AuthCa
        }
        If ($AuthCert){
          Add-Member -InputObject $update.distributor_configs.yum_distributor -MemberType NoteProperty -Name 'auth_cert' -Value $AuthCert
          Add-Member -InputObject $update.distributor_configs.export_distributor -MemberType NoteProperty -Name 'auth_cert' -Value $AuthCert
        }
        If ($PSBoundParameters.ContainsKey('GenerateSqlite')) {
          Add-Member -InputObject $update.distributor_configs.yum_distributor -MemberType NoteProperty -Name 'generate_sqlite' -Value $GenerateSqlite
          Add-Member -InputObject $update.distributor_configs.export_distributor -MemberType NoteProperty -Name 'generate_sqlite' -Value $GenerateSqlite
        }
        If ($PSBoundParameters.ContainsKey('ServeHttp')) {
          Add-Member -InputObject $update.distributor_configs.yum_distributor -MemberType NoteProperty -Name 'http' -Value $ServeHttp
          Add-Member -InputObject $update.distributor_configs.export_distributor -MemberType NoteProperty -Name 'http' -Value $ServeHttp
        }
        If ($PSBoundParameters.ContainsKey('ServeHttps')) {
          Add-Member -InputObject $update.distributor_configs.yum_distributor -MemberType NoteProperty -Name 'https' -Value $ServeHttps
          Add-Member -InputObject $update.distributor_configs.export_distributor -MemberType NoteProperty -Name 'https' -Value $ServeHttps
        }
      }
      $uri = "/pulp/api/v2/repositories/${repoId}"
      $body = ConvertTo-Json $update -Depth 20
      $null = Invoke-PulpRestMethod -Server $Server -Port $Port `
              -AuthenticationMethod $AuthenticationMethod `
              -Protocol $Protocol -Uri $uri -Method Put -body $body
      If (!$NoPublish) {
        $null = Publish-PulpRepo -Server $Server -Port $Port `
                -Protocol $Protocol `
                -AuthenticationMethod $AuthenticationMethod -Id $repoId -Type $Type
      }
      Get-PulpRepo -Server $Server -Port $Port `
                   -Protocol $Protocol -Id $repoId
    }
  }
}
