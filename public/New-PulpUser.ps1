# .ExternalHelp powershell-pulp-help.xml
Function New-PulpUser {
  [Cmdletbinding(DefaultParameterSetName='Strings')]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,
    
    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$true)]
    [string[]]$Login,
    
    [Parameter(Mandatory=$false)]
    [string]$Name,
    
    [Parameter(Mandatory=$false)]
    [string]$Password 
  )
  Begin { $uri = "/pulp/api/v2/users/" }
  Process {
    Foreach ($loginName in $Login) {
      $user = New-Object -TypeName PSCustomObject -Property @{
        'login'=$loginName;
        'password'= ([char[]](48..57) + [char[]](97..122) + [char[]](65..90) |
                    sort {get-random})[0..12] -join '';
        'name'=$loginName
      }
      if ($Name){ $user.name = $Name}
      if ($Password) { $user.password = $Password }
      Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
        -AuthenticationMethod $AuthenticationMethod -Uri $uri -Method Post `
        -Body (ConvertTo-Json $user)
    }
  }
}
