---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpRepo.md
schema: 2.0.0
---

# Get-PulpRepo

## SYNOPSIS
Get Pulp repositories.

## SYNTAX

```
Get-PulpRepo [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-Id] <String[]>] [-Type <String>] [<CommonParameters>]
```

## DESCRIPTION
This function gets Pulp repositories based on the repo ID provided.
Wildcards may be used. If no ID is provided, all repos will be returned.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpRepo
```

Get all Pulp repos.

### Example 2
```
PS C:\> Get-PulpRepo -Type rpm
```

Get all Pulp RPM repos.

### Example 3
```
PS C:\> Get-PulpRepo -Id epel
```

Get the `epel` Pulp repo.

### Example 4
```
PS C:\> Get-PulpRepo -Id 'epel*'
```

Get all Pulp repos with ID matching `epel*`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Type
Repo type.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Id
Pulp repo IDs.

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: False
Position: 0
Default value: *
Accept pipeline input: True (ByValue)
Accept wildcard characters: True
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

## NOTES
NAME:  Get-PulpRepo

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
