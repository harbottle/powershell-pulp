# .ExternalHelp powershell-pulp-help.xml
Function Remove-PulpSyncSchedule {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [PSCustomObject[]]$SyncSchedule,

    [Parameter(Mandatory=$false)]
    [string]$Type = '*'
  )
  Begin { If ($Type -eq 'rpm'){$Type = 'yum'} }
  Process {
    Foreach ($s in $SyncSchedule) {
      if ($s.resource.split(':')[3] -like "${Type}_importer") {
        $null = Invoke-PulpRestMethod -Server $Server -Port $Port `
          -Protocol $Protocol -AuthenticationMethod $AuthenticationMethod `
          -Uri $s.'_href' -Method Delete
      }
    }
  }
}
