# .ExternalHelp powershell-pulp-help.xml
Function New-PulpRepo {
 [Cmdletbinding(DefaultParameterSetName='Strings')]
 Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [object[]]$Repo,

    [Parameter(Mandatory=$true, Position=0, ParameterSetName="Strings")]
    [string]$Id,
    # API: id

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$DisplayName,
    # API: display_name

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Description,
    # API: description

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Note,
    # API: notes[]

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Feed,
    # API: importer_config.feed

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$Validate,
    # API: importer_config.validate

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$FeedCaCert,
    # API: importer_config.ssl_ca_cert

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$VerifyFeedSsl,
    # API: importer_config.ssl_validation

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$FeedCert,
    # API: importer_config.ssl_client_cert

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$FeedKey,
    # API: importer_config.ssl_client_key

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$ProxyHost,
    # API: importer_config.proxy_host

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$ProxyPort,
    # API: importer_config.proxy_port

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$ProxyUser,
    # API: importer_config.proxy_username

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$ProxyPass,
    # API: importer_config.proxy_password

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [int]$MaxDownloads,
    # API: importer_config.max_downloads

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [int]$MaxSpeed,
    # API: importer_config.max_speed

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$ServeHttp,
    # API (rpm): distributors[].distributor_config.http
    # API (iso and puppet): distributors[].distributor_config.serve_http
    # rpm default: false
    # iso default: not set
    # puppet default: not set

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$ServeHttps,
    # API (rpm): distributors[].distributor_config.https
    # API (iso and puppet): distributors[].distributor_config.serve_https
    # Default (rpm): true
    # Default (iso): not set
    # Default (puppet): not set

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$RemoveMissing,
    # Only: rpm, iso
    # API: importer_config.remove_missing

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$AuthCa,
    # Only: rpm, iso
    # API: distributors[].distributor_config.auth_ca

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$Skip,
    # Only: rpm
    # API: importer_config.type_skip_list[]
    # Valid: 'rpm', 'drpm', 'distribution', 'erratum'

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$BasicauthUser,
    # Only: rpm
    # API: importer_config.basic_auth_username

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$BasicauthPass,
    # Only: rpm
    # API: importer_config.basic_auth_password

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [int]$RetainOldCount,
    # Only: rpm
    # API: importer_config.retain_old_count

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$RelativeUrl,
    # Only: rpm
    # API: distributors[].distributor_config.relative_url

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$ChecksumType,
    # Only: rpm
    # API: distributors[].distributor_config.checksum_type

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$GpgKey,
    # Only: rpm
    # API: distributors[].distributor_config.gpgkey

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [bool]$GenerateSqlite,
    # Only: rpm
    # API: distributors[].distributor_config.generate_sqlite

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$HostCa,
    # Only: rpm
    # API: distributors[].distributor_config.https_ca

    [Parameter(Mandatory=$false, ParameterSetName="Strings")]
    [string]$AuthCert,
    # Only: rpm
    # API: distributors[].distributor_config.auth_cert

    [Parameter(Mandatory=$false)]
    [switch]$NoPublish,

    [Parameter(Mandatory=$true)]
    [string]$Type
  )
  Begin {}
  Process {
    $defaultRepo = @{}
    $defaultRepo.'rpm' = ConvertFrom-Json @'
      {
        "id":"__ID__",
        "display_name":null,
        "description":null,
        "notes":{
          "_repo-type":"rpm-repo"
        },
        "importer_type_id":"yum_importer",
        "importer_config":{},
        "distributors":[
          {
            "distributor_id":"yum_distributor",
            "distributor_type_id":"yum_distributor",
            "auto_publish":true,
            "distributor_config":{
              "http":false,
              "relative_url":"__RELATIVE_URL__",
              "https":true
            }
          },
          {
            "distributor_id":"export_distributor",
            "distributor_type_id":"export_distributor",
            "auto_publish":false,
            "distributor_config":{
              "http":false,
              "relative_url":"__RELATIVE_URL__",
              "https":true
            }
          }
        ]
      }
'@
    $defaultRepo.'iso' = ConvertFrom-Json @'
      {
        "id":"__ID__",
        "display_name":null,
        "description":null,
        "notes":{
          "_repo-type":"iso-repo"
        },
        "importer_type_id":"iso_importer",
        "importer_config":{},
        "distributors":[
          {
            "distributor_id":"iso_distributor",
            "distributor_type_id":"iso_distributor",
            "auto_publish":true,
            "distributor_config":{}
          }
        ]
      }
'@
    $defaultRepo.'puppet' = ConvertFrom-Json @'
      {
        "id":"_ID_",
        "display_name":null,
        "description":null,
        "notes":{
          "_repo-type":"puppet-repo"
        },
        "importer_type_id":"puppet_importer",
        "importer_config":{},
        "distributors":[
          {
            "distributor_id":"puppet_distributor",
            "distributor_type_id":"puppet_distributor",
            "auto_publish":true,
            "distributor_config":{}
          }
        ]
      }
'@
    If ($Repo) {
      Foreach ($repoItem in ($Repo |
               Where-Object {$_.notes.'_repo-type' -eq "${Type}-repo"}) ){
        $newRepo = $defaultRepo."${Type}"
        Foreach ($attribute in 'id','display_name','description','notes'){
          If ($repoItem.$attribute){
            $newRepo.$attribute = $repoItem.$attribute
          }
        }
        If ($repoItem.importers) {
          If ($repoItem.importers[0].importer_type_id){
            $newRepo.importer_type_id = $repoItem.importers[0].importer_type_id
          }
          If ($repoItem.importers[0].config){
            $newRepo.importer_config = $repoItem.importers[0].config
          }
        }
        For ($i=0; $i -le $newRepo.distributors.count; $i++){
          If ($repoItem.distributors | Where-Object {$_.distributor_type_id -eq $newRepo.distributors[$i].distributor_type_id}) {
            $newRepo.distributors[$i] = ($repoItem.distributors |
              Where-Object {$_.distributor_type_id -eq $newRepo.distributors[$i].distributor_type_id}) |
              Select-Object distributor_type_id, auto_publish,
              @{Name='distributor_config'; Expression={$_.config}},
              @{Name="distributor_id";Expression={$_."id"}}
          }
        }
        $uri = '/pulp/api/v2/repositories/'
        $body = ConvertTo-Json $newRepo -Depth 20
        $null = Invoke-PulpRestMethod -Server $Server -Port $Port `
                -AuthenticationMethod $AuthenticationMethod `
                -Protocol $Protocol -Uri $uri -Method Post -body $body
        If (!$NoPublish) {
          $null = Publish-PulpRepo -Server $Server -Port $Port `
                  -Protocol $Protocol `
                  -AuthenticationMethod $AuthenticationMethod -Id $newRepo.id `
                  -Type $Type
        }
        Get-PulpRepo -Server $Server -Port $Port `
                     -Protocol $Protocol -Id $newRepo.id
      }
    }
    Else {
      $newRepo = $defaultRepo."${Type}"

      # REST attributes for all repo types
      $newRepo.id = $Id
      If ($DisplayName) {$newRepo.display_name = $DisplayName}
      If ($Description) {$newRepo.description = $Description}
      If ($Note){
        Foreach ($noteItem in ($Note -replace '\s').split(",")){
          $noteName =  $noteItem.split("=")[0]
          $noteValue = $noteItem.split("=")[1]
          Add-Member -InputObject $newRepo.notes -MemberType NoteProperty -Name $noteName -Value $noteValue
        }
      }
      If ($Feed) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'feed' -Value $feed
      }
      If ($FeedCaCert) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'ssl_ca_cert' -Value $FeedCaCert
      }
      If ($FeedCert) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'ssl_client_cert' -Value $FeedCert
      }
      If ($FeedKey) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'ssl_client_key' -Value $FeedKey
      }
      If ($ProxyHost) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'proxy_host' -Value $ProxyHost
      }
      If ($ProxyPort) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'proxy_port' -Value $ProxyPort
      }
      If ($ProxyUser) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'proxy_username' -Value $ProxyUser
      }
      If ($ProxyPass) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'proxy_password' -Value $ProxyPass
      }
      If ($MaxDownloads) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'max_downloads' -Value $MaxDownloads
      }
      If ($MaxSpeed) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'max_speed' -Value $MaxSpeed
      }
      If ($PSBoundParameters.ContainsKey('Validate')) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'validate' -Value $Validate
      }
      If ($PSBoundParameters.ContainsKey('VerifyFeedSsl')) {
        Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'ssl_validation' -Value $VerifyFeedSsl
      }

      # REST attributes for iso and puppet repos only
      If ($Type -eq 'puppet' -or $Type -eq 'iso') {
        Foreach ($distributor in $newRepo.distributors) {
          If ($PSBoundParameters.ContainsKey('ServeHttp')) {
            Add-Member -Force -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'serve_http' -Value $ServeHttp
          }
          If ($PSBoundParameters.ContainsKey('ServeHttps')) {
            Add-Member -Force -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'serve_https' -Value $ServeHttps
          }
        }
      }

      # REST attributes for iso and rpm repos only
      If ($Type -eq 'rpm' -or $Type -eq 'iso') {
        If ($PSBoundParameters.ContainsKey('RemoveMissing')) {
          Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'remove_missing' -Value $RemoveMissing
        }
        Foreach ($distributor in $newRepo.distributors) {
          If ($auth_ca){
            Add-Member -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'auth_ca' -Value $AuthCa
          }
        }
      }

      # REST attributes for rpm repos only
      If ($Type -eq 'rpm') {
        If ($BasicauthUser) {
          Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'basic_auth_username' -Value $BasicauthUser
        }
        If ($BasicauthPass) {
          Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'basic_auth_password' -Value $BasicauthPass
        }
        If ($RetainOldCount) {
          Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'retain_old_count' -Value $RetainOldCount
        }
        If ($Skip) {
          Add-Member -InputObject $newRepo.importer_config -MemberType NoteProperty -Name 'skip' -Value ($Skip -replace '\s').split(',')
        }
        Foreach ($distributor in $newRepo.distributors) {
          $distributor.distributor_config.relative_url = $Id
          If ($Feed) {
            $distributor.distributor_config.relative_url = ($feed.split("/")[3..($feed.split("/").length)] -join "/")
          }
          If ($RelativeUrl){
            $distributor.distributor_config.relative_url = $RelativeUrl
          }
          If ($ChecksumType){
            Add-Member -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'checksum_type' -Value $ChecksumType
          }
          If ($GpgKey){
            Add-Member -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'gpgkey' -Value $GpgKey
          }
          If ($HostCa){
            Add-Member -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'https_ca' -Value $HostCa
          }
          If ($AuthCert){
            Add-Member -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'auth_cert' -Value $AuthCert
          }
          If ($PSBoundParameters.ContainsKey('GenerateSqlite')) {
            Add-Member -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'generate_sqlite' -Value $GenerateSqlite
          }
          If ($PSBoundParameters.ContainsKey('ServeHttp')) {
            Add-Member -Force -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'http' -Value $ServeHttp
          }
          If ($PSBoundParameters.ContainsKey('ServeHttps')) {
            Add-Member -Force -InputObject $distributor.distributor_config -MemberType NoteProperty -Name 'https' -Value $ServeHttps
          }
        }
      }
      $uri = '/pulp/api/v2/repositories/'
      $body = ConvertTo-Json $newRepo -Depth 20
      $null = Invoke-PulpRestMethod -Server $Server -Port $Port `
              -AuthenticationMethod $AuthenticationMethod `
              -Protocol $Protocol -Uri $uri -Method Post -body $body
      If (!$NoPublish) {
        $null = Publish-PulpRepo -Server $Server -Port $Port `
                -Protocol $Protocol `
                -AuthenticationMethod $AuthenticationMethod -Id $Id -Type $Type
      }
      Get-PulpRepo -Server $Server -Port $Port `
                   -Protocol $Protocol -Id $Id
    }
  }
}
