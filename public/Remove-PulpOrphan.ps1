# .ExternalHelp powershell-pulp-help.xml
Function Remove-PulpOrphan {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Orphan,

    [Parameter(Mandatory=$true, ValueFromPipeline=$false, ParameterSetName="Strings")]
    [switch]$All,

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {
    If ($Type -eq 'puppet'){ $Type = 'puppet_module' }
    If ($All) { # Strings
      If ($Type) {
        $uri = "/pulp/api/v2/content/orphans/${Type}/"
        Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
                              -AuthenticationMethod $AuthenticationMethod `
                              -Uri $uri -Method Delete
      } Else {
        $uri = "/pulp/api/v2/content/orphans/"
        Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
                              -AuthenticationMethod $AuthenticationMethod `
                              -Uri $uri -Method Delete
      }
    }
  }
  Process {
    If ($Orphan) { #Objects
      Foreach ($orphanItem in $Orphan) {
        If (!$Type -or $Type -eq $orphanItem._content_type_id) {
           Invoke-PulpRestMethod -Server $Server -Port $Port `
                                 -Protocol $Protocol `
                                 -AuthenticationMethod $AuthenticationMethod `
                                 -Uri $orphanItem._href -Method Delete
        }
      }
    }
  }
  End {}
}
