---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Remove-PulpUser.md
schema: 2.0.0
---

# Remove-PulpUser

## SYNOPSIS
Remove Pulp users.

## SYNTAX

```
Remove-PulpUser [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-User] <PSObject[]> [<CommonParameters>]
```

## DESCRIPTION
This function removes one or more users from a Pulp server. Pulp user objects
must be supplied.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpUser 'grace' | Remove-PulpUser
```

Remove user `grace`.

## PARAMETERS

### -Server
The Pulp server to query.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Server).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
The port to query.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
The protocol to use.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -User
Pulp user objects.

```yaml
Type: PSObject[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 1
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp user objects.

## OUTPUTS

### None

## NOTES
NAME:  Remove-PulpUser

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
