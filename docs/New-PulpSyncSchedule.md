---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/New-PulpSyncSchedule.md
schema: 2.0.0
---

# New-PulpSyncSchedule

## SYNOPSIS
Create new Pulp synchronization schedules.

## SYNTAX

### Strings (Default)
```
New-PulpSyncSchedule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -RepoId <String[]> -Schedule <String> [-FailureThreshold <Int32>] [-Type <String>] [<CommonParameters>]
```

### Objects
```
New-PulpSyncSchedule [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 -Repo <PSObject[]> -Schedule <String> [-FailureThreshold <Int32>] [-Type <String>] [<CommonParameters>]
```

## DESCRIPTION
This function creates new Pulp synchronization schedules for repos with an
upstream feed. Sync schedules can be applied to repos by supplying either repo
objects or repo IDs. Schedules must be supplied in iso8601 format.

## EXAMPLES

### Example 1
```
PS C:\> New-PulpSyncSchedule -RepoId centos6 -Schedule '1970-01-01T00:00:00Z/P1D'
```

Creates a new synchronization schedule for repository with ID `centos6` that
will run at midnight (UTC) daily.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Type
Repo type.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -RepoId
Pulp repo IDs.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Repo
Pulp repo objects.

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Schedule
Schedule in iso8601 including recurrence period.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: True
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -FailureThreshold
Set consecutive failures allowed before this scheduled sync is disabled.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp synchronization schedule objects.

## NOTES
NAME:  New-PulpSyncSchedule

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
