#!/bin/bash
export version=$(grep 'ModuleVersion' powershell-pulp.psd1 | sed "s/ModuleVersion = '\(.*\)'.*/\1/g")
export user=harbottle
export project=powershell-pulp
docker login -u ${user} -p ${docker_hub_password}
docker pull ${user}/${project}:latest || true
docker build . --build-arg version=${version} --pull --cache-from ${user}/${project}:latest -t ${user}/${project}:${version} -t ${user}/${project}:latest
