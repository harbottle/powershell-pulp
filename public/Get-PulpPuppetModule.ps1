# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpPuppetModule {
  [CmdletBinding(DefaultParameterSetName='Strings')]
  Param(
    [string]$Server,
    [int]$Port,
    [string]$Protocol,
    [string]$AuthenticationMethod,
    
    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$false)]
    [string]$Name = '*',

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Repo,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$RepoId
  )
  Begin {
    Try {
      $outBuffer = $null
      If ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'Get-PulpContentUnit',
        [System.Management.Automation.CommandTypes]::Function)
      $scriptCmd = {& $wrappedCmd @PSBoundParameters -Type 'puppet' }
      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    Catch {
      Throw
    }
  }
  Process {
    Try {
      $steppablePipeline.Process($_)
    }
    Catch {
      Throw
    }
  }
  End {
    Try {
      $steppablePipeline.End()
    }
    Catch {
      Throw
    }
  }
}
