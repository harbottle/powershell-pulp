# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpPuppetSyncSchedule {
  [CmdletBinding()]
  Param(
    [string]$Server,
    [int]$Port,
    [string]$Protocol,
    [string]$AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Objects")]
    [PSCustomObject[]]$Repo,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="Strings")]
    [string[]]$RepoId
  )
  Begin {
    try {
      $outBuffer = $null
      if ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'Get-PulpSyncSchedule', [System.Management.Automation.CommandTypes]::Function)
      $scriptCmd = {& $wrappedCmd @PSBoundParameters -Type 'puppet'}
      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    catch { throw }
  }
  Process {
    try {
      $steppablePipeline.Process($_)
    }
    catch {
      throw
    }
  }
  End {
    try {
      $steppablePipeline.End()
    }
    catch {
      throw
    }
  }
}
