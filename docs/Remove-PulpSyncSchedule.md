---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Remove-PulpSyncSchedule.md
schema: 2.0.0
---

# Remove-PulpSyncSchedule

## SYNOPSIS
Remove Pulp repo synchronization schedules.

## SYNTAX

```
Remove-PulpSyncSchedule [-Server <String>] [-Port <Int32>] [-Protocol <String>]
 [-AuthenticationMethod <String>] [-SyncSchedule] <PSObject[]> [-Type <String>] [<CommonParameters>]
```

## DESCRIPTION
This function removes Pulp repo synchronization schedules.  Synchronization
schedule objects must be supplied.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpRepo archive | Get-PulpSyncSchedule | Remove-PulpSyncSchedule
```

Remove all sync schedules for repo `archive`

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -Type
Repo type.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -SyncSchedule
Pulp sync schedule objects.

```yaml
Type: PSObject[]
Parameter Sets: (All)
Aliases:

Required: True
Position: 4
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSObject[]
Pulp synchronization schedule objects.

## OUTPUTS

### System.Object

## NOTES
NAME:  Remove-PulpSyncSchedule

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
