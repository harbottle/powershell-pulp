# .ExternalHelp powershell-pulp-help.xml
Function Remove-PulpContentUnit {
  [Cmdletbinding()]
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string]$Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
    [PSCustomObject[]]$ContentUnit,

    [Parameter(Mandatory=$false)]
    [switch]$NoPublish,

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {
    $repoIds = @()
    If ($Type -eq 'puppet'){ $Type = 'puppet_module' }
  }
  Process {
    $unitId = $ContentUnit | Select-Object -ExpandProperty metadata |
                             Select-Object -ExpandProperty _id
    $unitType = $ContentUnit | Select-Object -ExpandProperty unit_type_id
    $repoId = $ContentUnit | Select-Object -ExpandProperty repo_id
    $uri = "/pulp/api/v2/repositories/${repoId}/actions/unassociate/"
    If (!$Type -or $Type -eq $unitType) {
      $repoIds += $repoId
      $body = '{"criteria": {"filters": {"unit": {"_id": "' + $unitId + '"}}}}'
      Invoke-PulpRestMethod -Server $Server -Port $Port -Protocol $Protocol `
                            -AuthenticationMethod $AuthenticationMethod `
                            -Uri $uri -Method Post -Body $body
    }
  }
  End {
    If (!$NoPublish) {
      $repoIds | Select-Object -Unique |
                 Get-PulpRepo -Server $Server -Port $Port `
                              -Protocol ` $Protocol `
                              -AuthenticationMethod $AuthenticationMethod |
                 Publish-PulpRepo -Server $Server -Port $Port `
                                  -Protocol $Protocol `
                                  -AuthenticationMethod $AuthenticationMethod
    }
  }
}
