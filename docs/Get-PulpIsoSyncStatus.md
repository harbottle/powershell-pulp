---
external help file: powershell-pulp-help.xml
Module Name: powershell-pulp
online version: https://gitlab.com/harbottle/powershell-pulp/blob/master/docs/Get-PulpIsoSyncStatus.md
schema: 2.0.0
---

# Get-PulpIsoSyncStatus

## SYNOPSIS
Get Pulp ISO repo synchronization tasks.

## SYNTAX

### Strings (Default)
```
Get-PulpIsoSyncStatus [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [[-RepoId] <String[]>] [<CommonParameters>]
```

### Objects
```
Get-PulpIsoSyncStatus [-Server <String>] [-Port <Int32>] [-Protocol <String>] [-AuthenticationMethod <String>]
 [-Repo] <PSObject[]> [<CommonParameters>]
```

## DESCRIPTION
This function returns Pulp repo synchronization tasks for the ISO repo IDs or
ISO repo objects provided. If no IDs or repo objects are provided, all ISO repo
synchronization tasks will be returned.

## EXAMPLES

### Example 1
```
PS C:\> Get-PulpIsoRepo archive | Get-PulpIsoSyncStatus
```

Get all sync tasks for ISO repo `archive`.

## PARAMETERS

### -Server
Pulp server hostname or address.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Server
Accept pipeline input: False
Accept wildcard characters: False
```

### -Port
Pulp network port.

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Port).Port
Accept pipeline input: False
Accept wildcard characters: False
```

### -Protocol
Pulp network protocol.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -Protocol).Protocol
Accept pipeline input: False
Accept wildcard characters: False
```

### -AuthenticationMethod
Pulp authentication method (`basic` or `certificate`).

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod
Accept pipeline input: False
Accept wildcard characters: False
```

### -RepoId
Pulp repo IDs.

```yaml
Type: String[]
Parameter Sets: Strings
Aliases:

Required: False
Position: 0
Default value: *
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -Repo
Pulp repo type

```yaml
Type: PSObject[]
Parameter Sets: Objects
Aliases:

Required: True
Position: 0
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject[]
Pulp repo objects.

### System.String[]
Pulp repo IDs.

## OUTPUTS

### System.Management.Automation.PSCustomObject[]
Pulp task objects.

## NOTES
NAME:  Get-PulpIsoSyncStatus

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
