# .ExternalHelp powershell-pulp-help.xml
Function Add-PulpPuppetModule {
  [CmdletBinding(DefaultParameterSetName='StringsPath')]
  Param(
    [string]$Server,
    [int]$Port,
    [string]$Protocol,
    [string]$AuthenticationMethod,

    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="ObjectsPath")]
    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="ObjectsUrl")]
    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="ObjectsForge")]
    [PSCustomObject[]]$TargetRepo,

    [Parameter(Mandatory=$true, Position=1, ValueFromPipeline=$true, ParameterSetName="StringsPath")]
    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="StringsUrl")]
    [Parameter(Mandatory=$true, ValueFromPipeline=$true, ParameterSetName="StringsForge")]
    [string[]]$TargetRepoId,

    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$false, ParameterSetName="ObjectsPath")]
    [Parameter(Mandatory=$true, Position=0, ValueFromPipeline=$false, ParameterSetName="StringsPath")]
    [string[]]$Path,

    [Parameter(Mandatory=$true, ValueFromPipeline=$false, ParameterSetName="ObjectsUrl")]
    [Parameter(Mandatory=$true, ValueFromPipeline=$false, ParameterSetName="StringsUrl")]
    [string]$Url,

    [Parameter(Mandatory=$false, ParameterSetName='ObjectsForge')]
    [Parameter(Mandatory=$false, ParameterSetName='StringsForge')]
    [string]$ForgeBaseUrl = 'https://forge.puppet.com/v3/files/',

    [Parameter(Mandatory=$true, Position=0, ParameterSetName='ObjectsForge')]
    [Parameter(Mandatory=$true, Position=0, ParameterSetName='StringsForge')]
    [string]$Author,

    [Parameter(Mandatory=$true, Position=1, ParameterSetName='ObjectsForge')]
    [Parameter(Mandatory=$true, Position=1, ParameterSetName='StringsForge')]
    [string]$Name,

    [Parameter(Mandatory=$true, Position=2, ParameterSetName='ObjectsForge')]
    [Parameter(Mandatory=$true, Position=2, ParameterSetName='StringsForge')]
    [string]$Version,

    [Parameter(Mandatory=$false)]
    [switch]$NoPublish
  )
  Begin {
    try {
      $outBuffer = $null
      if ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
        $PSBoundParameters['OutBuffer'] = 1
      }
      $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand(
        'Add-PulpContentUnit', [System.Management.Automation.CommandTypes]::Function)
      If ($Author) {
        $Url = "${ForgeBaseUrl}${Author}-${Name}-${Version}.tar.gz"
        Foreach ($p in 'ForgeBaseUrl', 'Author', 'Name', 'Version') {
          $null = $PSBoundParameters.Remove($p)
        }
        $scriptCmd = {& $wrappedCmd @PSBoundParameters -Url $Url -Type 'puppet'}
      } Else {
        $scriptCmd = {& $wrappedCmd @PSBoundParameters -Type 'puppet' }
      }
      $steppablePipeline = $scriptCmd.GetSteppablePipeline()
      $steppablePipeline.Begin($PSCmdlet)
    }
    catch { throw }
  }
  Process {
    try {
      $steppablePipeline.Process($_)
    }
    catch {
      throw
    }
  }
  End {
    try {
      $steppablePipeline.End()
    }
    catch {
      throw
    }
  }
}
