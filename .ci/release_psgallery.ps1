New-Item -Type Directory -Force module\powershell-pulp
Copy-Item * .\module\powershell-pulp -Recurse -Exclude (Get-Content .\.pkgignore)
cd module\powershell-pulp
Publish-Module -Path .\ -NuGetApiKey $env:NuGetApiKey
