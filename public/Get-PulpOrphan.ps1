# .ExternalHelp powershell-pulp-help.xml
Function Get-PulpOrphan {
  Param(
    [Parameter(Mandatory=$false)]
    [string]$Server = (Get-PulpLocalConfig -Server).Server,

    [Parameter(Mandatory=$false)]
    [int]$Port = (Get-PulpLocalConfig -Port).Port,

    [Parameter(Mandatory=$false)]
    [string] $Protocol = (Get-PulpLocalConfig -Protocol).Protocol,

    [Parameter(Mandatory=$false)]
    [string]$AuthenticationMethod = (Get-PulpLocalConfig -AuthenticationMethod).AuthenticationMethod,

    [Parameter(Mandatory=$false, Position=0, ValueFromPipeline=$true)]
    [string[]]$Name = '*',

    [Parameter(Mandatory=$false)]
    [string]$Type
  )
  Begin {
    If ($Type -eq 'puppet') {
      $Type = 'puppet_module'
    }
    # For puppet modules, break name into author, name, version
    $modAuthor = $Name.split("-")[0]
    $modName = if ($Name.split("-")[1] -ne $null) {$Name.split("-")[1]} else {'*'}
    $modVersion = if ($Name.split("-")[2] -ne $null) {$Name.split("-")[2]} else {'*'}
    If ($Type) {
      $uri = "/pulp/api/v2/content/orphans/${Type}/"
      $orphans = Invoke-PulpRestMethod -Server $Server -Port $Port `
                 -Protocol $Protocol `
                 -AuthenticationMethod $AuthenticationMethod -Uri $uri `
                 -Method Get -TimeoutSec 3600
    } Else {
      $uri = "/pulp/api/v2/content/orphans/"
      Write-Progress -activity "Getting Pulp orphans" `
                     -status "Getting information about available orphan types"
      $summaries = Invoke-PulpRestMethod -Server $Server -Port $Port `
                   -Protocol $Protocol `
                   -AuthenticationMethod $AuthenticationMethod -Uri $uri `
                   -Method Get -TimeoutSec 3600
      $types = $summaries | Get-Member -MemberType NoteProperty |
               Select-Object -ExpandProperty Name
      $i = 0
      Foreach ($type in $types) {
        Write-Progress -activity "Getting Pulp orphans" `
                       -status "Getting ${type} orphans" `
                       -percentComplete ($i / $types.length * 100)
        $orphans += Invoke-PulpRestMethod -Server $Server -Port $Port `
                 -Protocol $Protocol `
                 -AuthenticationMethod $AuthenticationMethod `
                 -Uri $summaries.$type._href `
                 -Method Get -TimeoutSec 3600
        $i++
       }
    }
  }
  Process {
    If ($Name) {
      $orphans | Where-Object {
      ( # puppet orphans are filtered by author-name-version
          ($_._content_type_id -eq 'puppet_module') -and
          ($_.author -like $modAuthor) -and
          ($_.name -like $modName) -and
          ($_.version -like $modVersion)
        ) -or
        ( # anything else we will just filter by name (orphans don't have filename for rpm)
          ($_._content_type_id -ne 'puppet_module') -and
          ($_.name -like $Name)
        )
      }
    } Else { $orphans }
  }
}
